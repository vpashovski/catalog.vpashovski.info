<?php

return [
    //BACKEND
    'per_page'              => 20,
    'dashboard_records'     => 5,

    //FRONTEND
    'index_page_products'   => 8,
    'category_page_limit'   => 9,
    'carousel_items'        => 8,
    'days_older_than'       => 3,
    'visited_items'         => 3,
    'recent_articles_limit' => 5,
    'articles_page_limit'   => 5,
    'shop_page_limit'       => 12,
    'footer_pages_limit'    => 14,
//    'most_read_articles'    => 10,
//    'most_liked_articles'   => 10,
//    'index_page_articles'   => 10,
//    'polls_per_page'        => 10,

    //CONFIG
//    'max_votes_from_ip'     => 10,
];
