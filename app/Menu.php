<?php

namespace App;

class Menu extends Model
{
    protected $fillable = ['title', 'url', 'order'];
}
