<?php

namespace App;

use File;

class Sync
{
    /**
     * @param int $shop_id
     */
    public static function run($shop_id = 0) {
        $shop = Shop::find($shop_id);

        if ($shop && $shop->status) {
            ini_set('memory_limit', '1024M');
            ini_set('max_execution_time', 20000);

            $log['shop_id'] = $shop_id;
            $log['total_inserted_products'] = 0;
            $log['total_updated_products'] = 0;
            $log['total_deleted_products'] = 0;
            $log['total_error_products'] = 0;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, trim($shop->feed_url));
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
            $xml_data = curl_exec($curl);
            curl_close($curl);

            if ($xml_data) {
                $xml = (array)simplexml_load_string($xml_data);
            } else {
                $xml = '';
            }

            if (!empty($xml)) {
                $old_products = array();
                foreach ($shop->products as $shop_product) {
                    $old_products[$shop_product->id] = $shop_product;
                }

                $updated_products = array();

                $attribute_set = AttributeSet::find($shop->attribute_set_id);

                if ($attribute_set) {
                    $count = count($xml[$attribute_set->xml_product_tag]);

                    $aff_link_tag = $attribute_set->attributes()->where('key', 'like', Product::AFF_LINK)->first();

                    $current = 0;
                    foreach ($xml[$attribute_set->xml_product_tag] as $xml_product) {
                        $current++;
                        $product_data = array();
                        $slug = '';

                        $old_product = null;

                        foreach ($attribute_set->attributes as $attribute) {
                            if ($attribute->key == Product::NAME) {
                                $slug = slugify($xml_product->{$attribute->pivot->xml_tag} . '-' . hash('md5', $xml_product->{$aff_link_tag->pivot->xml_tag}));
                                $old_product = $shop->products()->where('slug', 'like', $slug)->first();
                                if ($old_product) {
                                    $products_for_update[$old_product->id] = $old_product;
                                }
                            }
                            $product_data[$attribute->key] = $xml_product->{$attribute->pivot->xml_tag};
                        }

                        if ($old_product) {
                            foreach ($product_data as $key => $value) {
                                foreach ($product_data as $key => $value) {
                                    if ($key != Product::THUMB
                                        && $key != Product::IMAGES
                                        && $key != Product::CATEGORY
                                    ) {
                                        $old_product->setData($key, $value);
                                    }
                                }
                            }

                            foreach ($old_product->attributeKeys() as $existings_attribute) {
                                if (!isset($product_data[$existings_attribute])) {
                                    $old_product->unsData($existings_attribute);
                                }
                            }

                            $updated_products[$old_product->id] = $old_product;
                            $log['total_updated_products']++;
                        } else {

                            // TODO upload images
                            $product = Product::create([
                                'slug'              => $slug,
                                'shop_id'           => $shop_id,
                                'attribute_set_id'  => $attribute_set->id
                            ]);

                            foreach ($product_data as $key => $value) {
                                if ($key == Product::THUMB) {
                                    self::uploadImage($product, $value);
                                } elseif ($key == Product::IMAGES) {
                                    $gallery = explode(',', $value);
                                    foreach ($gallery as $img) {
                                        self::uploadImage($product, $img);
                                    }
                                } elseif ($key == Product::CATEGORY) {
                                    self::productToCategory($product, $value);
                                } else {
                                    $product->setData($key, $value);
                                }
                            }

                            $log['total_inserted_products']++;
                        }
                        self::progress(['current' => $current , 'count' => $count, 'log' => null]);
                    }

                    foreach ($old_products as $id => $old) {
                        if (!isset($updated_products[$id])) {
                            $old->unsData('*');
                            $old->delete();
                            $log['total_deleted_products']++;
                        }
                    }
                }

                Log::create($log);

                self::progress(['current' => $current , 'count' => $count, 'log' => $log]);
            }
        } else {
            return sprintf('Shop with ID %s is not sync!', $shop_id);
        }
    }

    protected static function uploadImage($product, $image)
    {
        $file_headers = @get_headers($image);
        if (is_array($file_headers)
            && (in_array('Content-Type: image/jpeg', $file_headers)
                || in_array('Content-Type: image/png', $file_headers)
                || in_array('Content-Type: image/gif', $file_headers))
        ) {
            $image_path = ProductImage::$filepath . '/' . $product->shop_id;
            if (!File::isDirectory($image_path)) {
                File::makeDirectory($image_path, 0777);
            }
            $image_name = $image_path . '/' . File::name($image) . '.' . File::extension($image);
            if (!File::isFile($image_name)) {
                File::copy($image, $image_name);

                $product_image = ProductImage::create([
                    'title' => $product->shop_id . '/' . File::name($image),
                    'ext'   => File::extension($image)
                ]);

                $product->image_id = $product_image->id;
                $product->save();
                $product_image->products()->attach($product->id);
            }
        }
    }

    protected static function productToCategory($product, $category_name)
    {
        $category = Category::where([
            ['slug', 'like', slugify($category_name)],
            ['parent_category_id', '=', $product->shop->category_id],
        ])->first();
        if (!$category) {
            $category = Category::create([
                'name'                  => $category_name,
                'slug'                  => slugify($category_name),
                'parent_category_id'    => $product->shop->category_id,
            ]);
        }

        $product->categories()->attach($category->id);
    }

    protected static function progress($data) {
        if (ob_get_status()) {
            echo "data: " . json_encode($data) . PHP_EOL;
            echo PHP_EOL;

            ob_flush();
            flush();
        }
    }
}
