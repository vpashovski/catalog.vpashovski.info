<?php

namespace App;

use Request;

class AttributeSet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'xml_product_tag'
    ];

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class)->withTimestamps()->withPivot('xml_tag');
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }
}
