<?php

namespace App;

use Request;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commentator_name', 'commentator_email', 'comment', 'approved', 'article_id'
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
