<?php

namespace App;

use DB;
use Request;

class Stat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['views', 'ip_address'];

    public function products()
    {
        return $this->morphMany(Product::class, 'statable');
    }

    public function artiles()
    {
        return $this->morphMany(Article::class, 'statable');
    }

    public function shops()
    {
        return $this->morphMany(Shop::class, 'statable');
    }

    public static function getChartData($class)
    {
        $result = [];
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime("-$i days"));
            $query = DB::table('stats')
                ->select(DB::raw('COUNT(*) as total_visits'))
                ->where('statable_type', 'LIKE', str_replace('\\', '\\\\', $class))
                ->whereDate('updated_at', '=', $date)
                ->groupBy('statable_type')
                ->first();

            $result[strtotime($date) * 1000] = $query ? $query->total_visits : 0;
        }
        return $result;
    }

    public static function getFilteredResults($class)
    {
        $query = DB::table('stats')
            ->select(DB::raw('`statable_id` as item_id, COUNT(*) as unique_views, SUM(`views`) as total_views, MAX(`updated_at`) as last_visited'))
            ->where('statable_type', 'LIKE', str_replace('\\', '\\\\', $class))
            ->groupBy('statable_id');

        if (Request::has('item_id')) {
            $query = $query->where('statable_id', '=', Request::input('item_id'));
        }

        if (Request::has('unique_views')) {
            $query->havingRaw('COUNT(*) = ?', [Request::input('unique_views')]);
        }

        if (Request::has('total_views')) {
            $query->havingRaw('SUM(`views`) = ?', [Request::input('total_views')]);
        }

        if (Request::has('last_visited')) {
            $query->havingRaw('MAX(`updated_at`) LIKE ?', ['%' . Request::input('last_visited') . '%']);
        }

        if (Request::has('order_by') || Request::has('order')) {
            $query = $query->orderBy(
                Request::has('order_by') ? Request::input('order_by') : 'item_id',
                Request::has('order') ? Request::input('order') : 'desc'
            );
        }

        return $query->paginate(config('constant.per_page'));
    }

}
