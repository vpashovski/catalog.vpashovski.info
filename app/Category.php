<?php

namespace App;

use Request;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'parent_category_id'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_category_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(static::class, 'parent_category_id');
    }

    public function setParentCategoryIdAttribute($value)
    {
        if ($value == '') {
            $this->attributes['parent_category_id'] = null;
        } else {
            $this->attributes['parent_category_id'] = $value;
        }
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = slugify($value);
    }
}
