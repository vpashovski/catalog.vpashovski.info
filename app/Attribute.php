<?php

namespace App;

use Request;

class Attribute extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'name', 'type', 'visible_in_front'
    ];

    const ALL_ATTRIBUTES = '*';

    public function attribute_sets()
    {
        return $this->belongsToMany(AttributeSet::class)->withTimestamps()->withPivot('xml_tag');
    }
}
