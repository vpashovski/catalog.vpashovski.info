<?php

namespace App;

use Request;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'content', 'in_footer'
    ];

    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = slugify($value);
    }
}
