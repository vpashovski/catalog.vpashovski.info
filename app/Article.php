<?php

namespace App;

use Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description', 'content', 'section_id'
    ];

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable')->withTimestamps();
    }

    public function getImageAttribute($value)
    {
        return $this->images()->first();
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stats()
    {
        return $this->morphMany(Stat::class, 'statable');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = slugify($value);
    }

    public function getTagListAttribute()
    {
        return $this->tags()->pluck('id')->toArray();
    }

    public function getApprovedCommentsAttribute()
    {
        return $this->comments()->where('approved', '=', '1')->get();
    }

    public function getRatingAttribute()
    {
        return ($this->rate_count) ? $this->rate / $this->rate_count : 0;
    }

    public function getViewsAttribute()
    {
        $views = 0;
        foreach ($this->stats as $stat) {
            $views += $stat->views;
        }

        return $views;
    }

    public function getUniqueViewsAttribute()
    {
        return $this->stats()->count();
    }
}
