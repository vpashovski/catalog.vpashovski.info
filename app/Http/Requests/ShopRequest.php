<?php

namespace App\Http\Requests;

class ShopRequest extends Request
{
    protected $modelName = 'shop';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|max:255',
            'slug'              => 'required|max:255|unique:shops,slug' . $this->idOrEmpty,
            'feed_url'          => 'required|max:255|url',
            'shop_url'          => 'max:255|url',
            'shop_aff_url'      => 'max:255|url',
            'attribute_set_id'  => 'required|integer|exists:attribute_sets,id',
            'category_id'       => 'required|integer|exists:categories,id',
            'image_id'          => 'integer|exists:images,id',
            'cron_schedule'     => 'max:20',
            'status'            => 'required|boolean',
        ];
    }
}
