<?php

namespace App\Http\Requests;

class CommentRequest extends Request
{
    protected $modelName = 'comment';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commentator_name'  => 'required|max:255',
            'commentator_email' => 'email|max:255',
            'approved'          => 'required|boolean',
            'article_id'        => 'required|exists:articles,id',
            'comment'           => 'required',
        ];
    }
}
