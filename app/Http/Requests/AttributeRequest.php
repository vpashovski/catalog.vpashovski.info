<?php

namespace App\Http\Requests;

class AttributeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key'               => 'required|max:255',
            'name'              => 'required|max:255',
            'type'              => 'required',
            'visible_in_front'  => 'required|boolean',
        ];
    }
}
