<?php

namespace App\Http\Controllers;

use View;
use App\Product;
use App\Shop;
use App\Category;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        View::share('products_count', Product::all()->count());
        View::share('categories_count', Category::all()->count());
        View::share('shops_count', Shop::all()->count());
        View::share('unchecked_comments_count', Comment::where('checked', '=', 0)->count());
    }
}
