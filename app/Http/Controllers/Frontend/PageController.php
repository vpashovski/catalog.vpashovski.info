<?php

namespace App\Http\Controllers\Frontend;

use App\Page;

class PageController extends \App\Http\Controllers\FrontendController
{

    /**
     * @param $url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($url)
    {
        $page = Page::where('url', $url)->first();

        if (is_null($page)) {
            return redirect()->route('index');
        }

        return view('frontend.pages.page', compact('page'));
    }
}
