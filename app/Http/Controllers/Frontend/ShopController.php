<?php

namespace App\Http\Controllers\Frontend;

use View;
use App\Stat;
use App\Shop;
use App\Http\Requests\ShopRateRequest;

class ShopController extends \App\Http\Controllers\FrontendController
{

    public function index()
    {
        $shops = Shop::where('status', 1)->paginate(config('constant.shop_page_limit'));

        return view('frontend.pages.shop.index', compact('shops'));
    }

    /**
     * @param $shop_slug
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function view($shop_slug)
    {
        $shop = Shop::where([
            ['slug', 'like', $shop_slug],
            ['status', '=', 1],
        ])->first();

        if (is_null($shop)) {
            return redirect()->route('index');
        }

        $carousel_products = $shop->products()->with(['images'])
            ->ordered()->paginate(config('constant.carousel_items'));

        $left_shops = Shop::where('status', 1)->get();
        View::share('left_shops', $left_shops);

        $stat = $shop->stats()->where('ip_address', 'like', request()->ip())->first();

        if ($stat) {
            $stat->views++;
            $stat->save();
        } else {
            $stat = new Stat([
                'views'         => 1,
                'ip_address'    => request()->ip()
            ]);
            $shop->stats()->save($stat);
        }

        return view('frontend.pages.shop.view', compact('shop', 'carousel_products'));
    }

    /**
     * @param $shop_slug
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function products($shop_slug)
    {
        $shop = Shop::where([
            ['slug', 'like', $shop_slug],
            ['status', '=', 1],
        ])->first();

        if (is_null($shop)) {
            return redirect()->route('index');
        }

        $products = $shop->products()->with(['images'])
            ->ordered()->paginate(config('constant.category_page_limit'));

        $left_shops = Shop::where('status', 1)->get();
        View::share('left_shops', $left_shops);

        return view('frontend.pages.shop.products', compact('shop', 'products'));
    }

    public function rate(ShopRateRequest $request)
    {
        $shop = Shop::find($request->input('shop_id'));

        if (is_null($shop)) {
            return response()->json([
                'success'   => false,
                'message'   => trans('frontend_shop.shop_error')
            ], 422);
        }

        $rated_shops = $request->cookie('rated_shops', []);

        if (! is_array($rated_shops)) {
            $rated_shops = [];
        }

        if (in_array($shop->id, $rated_shops)) {
            return response()->json([
                'success'   => false,
                'message'   => trans('frontend_shop.rate_error')
            ], 422);
        }
        $rated_shops[] = $shop->id;

        $shop->rate += $request->input('value');
        $shop->rate_count += 1;
        $shop->save();

        //set cookie expire time to one year
        $cookie = cookie('rated_shops', $rated_shops, 525600);

        return response()->json([
            'success'   => true,
            'rating'    => $shop->rating,
            'message'   => trans('frontend_shop.rate_success')
        ])->withCookie($cookie);
    }
}
