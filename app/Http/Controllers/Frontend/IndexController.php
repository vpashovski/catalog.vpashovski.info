<?php

namespace App\Http\Controllers\Frontend;

use App\Product;

class IndexController extends \App\Http\Controllers\FrontendController
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(['image'])
            ->latest()->limit(config('constant.index_page_products'))
            ->get();

        $carousel_products = Product::with(['image'])
            ->inRandomOrder()->limit(config('constant.carousel_items'))
            ->get();

        return view('frontend.pages.index', compact('products', 'carousel_products'));
    }
}
