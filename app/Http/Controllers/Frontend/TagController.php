<?php

namespace App\Http\Controllers\Frontend;

use App\Tag;

class TagController extends \App\Http\Controllers\FrontendController
{
    /**
     * @param string $tag_slug
     * @return \Illuminate\Http\Response
     */
    public function index($tag_slug)
    {
        $tag = Tag::where('slug', $tag_slug)->first();

        if (is_null($tag)) {
            return redirect()->route('index');
        }

        $title = $tag->title;

        $articles = $tag->articles()->with(['section', 'images', 'user', 'tags'])
            ->ordered()->paginate(config('constant.articles_page_limit'));

        return view('frontend.pages.section', compact('title', 'articles'));
    }
}
