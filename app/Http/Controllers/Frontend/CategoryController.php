<?php

namespace App\Http\Controllers\Frontend;

use View;
use App\Product;
use App\Category;

class CategoryController extends \App\Http\Controllers\FrontendController
{
    /**
     * @param $slug
     * @param null $sub_slug
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function index($slug, $sub_slug = null)
    {
        $category = Category::where('slug', $slug)->first();
        $sub_category = null;

        if (is_null($category)) {
            return redirect()->route('index');
        }

        View::share('parent_category', $category);

        if (!$sub_slug) {
            $ids[] = $category->id;
            foreach ($category->childrens as $child) {
                $ids[] = $child->id;
            }

            $products = Product::with(['image'])->whereHas('categories', function($query) use($ids) {
                $query->whereIn('id', $ids);
            })->ordered()->paginate(config('constant.category_page_limit'));
        } else {
            $sub_category = Category::where([
                ['slug', 'like', $sub_slug],
                ['parent_category_id', '=', $category->id]
            ])->first();

            if (is_null($sub_category)) {
                return redirect()->route('index');
            }

            $products = $sub_category->products()->with(['images'])
                ->ordered()->paginate(config('constant.category_page_limit'));

            $category = $sub_category;
        }

        $carousel_products = Product::with(['image'])
            ->inRandomOrder()->limit(config('constant.carousel_items'))
            ->get();

        return view('frontend.pages.category', compact('category', 'products', 'carousel_products'));
    }
}
