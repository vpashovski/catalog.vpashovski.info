<?php

namespace App\Http\Controllers\Frontend;

use View;
use App\Stat;
use App\Product;

class ProductController extends \App\Http\Controllers\FrontendController
{
    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index($id)
    {
        $product = Product::find($id);

        if (is_null($product)) {
            return redirect()->route('index');
        }
        if ($product->categories->first()) {
            $category = $product->categories->first();
            $carousel_products = $category->products()
                ->limit(config('constant.carousel_items'))
                ->get();
        } else {
            $carousel_products = Product::with(['image'])
                ->inRandomOrder()->limit(config('constant.carousel_items'))
                ->get();
        }

        $stat = $product->stats()->where('ip_address', 'like', request()->ip())->first();

        if ($stat) {
            $stat->views++;
            $stat->save();
        } else {
            $stat = new Stat([
                'views'         => 1,
                'ip_address'    => request()->ip()
            ]);
            $product->stats()->save($stat);
        }

        if ($product->categories->first()->parent) {
            View::share('parent_category', $product->categories->first()->parent);
        }

        return view('frontend.pages.product', compact('product', 'carousel_products'));
    }
}
