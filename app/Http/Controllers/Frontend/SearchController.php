<?php

namespace App\Http\Controllers\Frontend;

use DB;
use App\Product;
use App\Attribute;

class SearchController extends \App\Http\Controllers\FrontendController
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = \Request::get('q');
        $attribute = Attribute::where('key', 'like', 'name')->first();
        $productIds = [];
        $result = DB::table('attribute_value_varchar')->select('product_id', 'value')->where('attribute_id', '=', $attribute->id)->get();
        foreach ($result as $item) {
            similar_text($item->value, $search, $percent);
            if ($percent >= 60) {
                $productIds[] = $item->product_id;
            }
        }

        $products = Product::with(['image'])->whereIn('id', $productIds)
            ->ordered()->paginate(config('constant.category_page_limit'));

        return view('frontend.pages.search', compact('products'));
    }
}
