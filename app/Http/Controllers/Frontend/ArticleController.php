<?php

namespace App\Http\Controllers\Frontend;

use Auth;
use App\Stat;
use App\Article;
use App\Section;
use App\Http\Requests\ArticleRateRequest;
use App\Http\Requests\ArticleCommentRequest;

class ArticleController extends \App\Http\Controllers\FrontendController
{
    /**
     * @param string $section_slug
     * @param string $article_slug
     * @return \Illuminate\Http\Response
     */
    public function index($section_slug, $article_slug)
    {
        $section = Section::where('slug', $section_slug)->first();

        if (is_null($section)) {
            return redirect()->route('index');
        }

        $article = Article::where('slug', $article_slug)
            ->where('section_id', $section->id)->first();

        if (is_null($article)) {
            return redirect()->route('index');
        }

        $stat = $article->stats()->where('ip_address', 'like', request()->ip())->first();

        if ($stat) {
            $stat->views++;
            $stat->save();
        } else {
            $stat = new Stat([
                'views'         => 1,
                'ip_address'    => request()->ip()
            ]);
            $article->stats()->save($stat);
        }

        return view('frontend.pages.article', compact('article'));
    }

    public function comment(ArticleCommentRequest $request)
    {
        $article = Article::find($request->input('article_id'));

        if (is_null($article)) {
            return response()->json([
                'success'   => false,
                'message'   => trans('frontend_blog.article_error')
            ], 422);
        }

        $article->comments()->create($request->all());

        return response()->json([
            'success'   => true,
            'message'   => trans('frontend_blog.comment_success')
        ]);
    }

    public function rate(ArticleRateRequest $request)
    {
        $article = Article::find($request->input('article_id'));

        if (is_null($article)) {
            return response()->json([
                'success'   => false,
                'message'   => trans('frontend_blog.article_error')
            ], 422);
        }

        $rated_articles = $request->cookie('rated_articles', []);

        if (! is_array($rated_articles)) {
            $rated_articles = [];
        }

        if (in_array($article->id, $rated_articles)) {
            return response()->json([
                'success'   => false,
                'message'   => trans('frontend_blog.rate_error')
            ], 422);
        }
        $rated_articles[] = $article->id;

        $article->rate += $request->input('value');
        $article->rate_count += 1;
        $article->save();

        //set cookie expire time to one year
        $cookie = cookie('rated_articles', $rated_articles, 525600);

        return response()->json([
            'success'   => true,
            'rating'    => $article->rating,
            'message'   => trans('frontend_blog.rate_success')
        ])->withCookie($cookie);
    }
}
