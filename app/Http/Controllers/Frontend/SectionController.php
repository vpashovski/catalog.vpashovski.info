<?php

namespace App\Http\Controllers\Frontend;

use App\Section;

class SectionController extends \App\Http\Controllers\FrontendController
{
    /**
     * @param string $section_slug
     * @return \Illuminate\Http\Response
     */
    public function index($section_slug)
    {
        $section = Section::where('slug', $section_slug)->first();

        if (is_null($section)) {
            return redirect()->route('index');
        }

        $title = $section->title;

        $articles = $section->articles()->with(['section', 'images', 'user', 'tags'])
            ->ordered()->paginate(config('constant.articles_page_limit'));

        return view('frontend.pages.section', compact('title', 'articles'));
    }
}
