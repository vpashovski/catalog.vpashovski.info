<?php

namespace App\Http\Controllers\Frontend;

use Mail;
use App\Http\Requests\ContactRequest;

class ContactController extends \App\Http\Controllers\FrontendController
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.pages.contact');
    }

    public function send(ContactRequest $request)
    {
        Mail::raw($request->input('comment'), function($message) use ($request) {

            $message->from($request->input('email'), $request->input('name'));

            $message->to(\App\Setting::get('contact_email'));

            $message->subject(trans('frontend_contact.contact_subject'));

        });

        return redirect()->route('contact')
            ->with('success', trans('frontend_contact.contact_success'));
    }
}
