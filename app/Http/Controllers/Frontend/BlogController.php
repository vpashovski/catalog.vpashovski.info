<?php

namespace App\Http\Controllers\Frontend;

use View;
use App\Article;
use Illuminate\Http\Request;

class BlogController extends \App\Http\Controllers\FrontendController
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = trans('frontend_blog.index');

        $articles = Article::with(['section', 'images', 'user', 'tags'])
            ->ordered()->paginate(config('constant.articles_page_limit'));

        return view('frontend.pages.section', compact('title', 'articles'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function search(Request $request)
    {
        if (!$request->has('query')) {
            return redirect()->route('index');
        }

        $title = trans('frontend_blog.search_for') . ' ' . $request->input('query');

        $articles = Article::with(['section', 'images', 'user', 'tags'])
            ->orWhere('title', 'like', '%' . $request->input('query') . '%')
            ->orWhere('slug', 'like', '%' . $request->input('query') . '%')
            ->orWhere('description', 'like', '%' . $request->input('query') . '%')
            ->orWhere('content', 'like', '%' . $request->input('query') . '%')
            ->ordered()->paginate(config('constant.articles_page_limit'));

        return view('frontend.pages.section', compact('title', 'articles'));
    }
}
