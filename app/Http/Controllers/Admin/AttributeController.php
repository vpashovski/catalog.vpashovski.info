<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Attribute;
use App\Http\Requests\AttributeRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class AttributeController extends AdminController
{
    protected $breadcrumbs = [];

    protected $types = [
        'int',
        'varchar',
        'decimal',
        'datetime',
        'text',
    ];

    public function __construct()
    {
        parent::__construct();

        View::share('title', trans('admin_attribute.index'));

        View::share('types', $this->getTypes());

        $this->breadcrumbs['admin.attribute.index'] = trans('admin_attribute.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.attribute.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\AttributeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeRequest $request)
    {
        Attribute::create($request->all());

        return redirect()->route('admin.attribute.index')
            ->with('success', trans('admin_attribute.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        return view('admin.pages.attribute.show', compact('attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute)
    {
        return view('admin.pages.attribute.edit', compact('attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\AttributeRequest  $request
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeRequest $request, Attribute $attribute)
    {
        $attribute->update($request->all());

        return redirect()->route('admin.attribute.index')
            ->with('success', trans('admin_attribute.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        $attribute->delete();

        return redirect()->route('admin.attribute.index')
            ->with('success', trans('admin_attribute.success.delete'));
    }

    public function getTypes()
    {
        return array_combine($this->types, $this->types);
    }

    public function allAttributes(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'key',
            3 => 'type',
            4 => 'actions',
        );

        $totalData = Attribute::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $attributes = Attribute::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $attributes =  Attribute::where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('key', 'LIKE', "%{$search}%")
                ->orWhere('type', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Attribute::where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('key', 'LIKE', "%{$search}%")
                ->orWhere('type', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = [];

        if(!empty($attributes)) {
            foreach ($attributes as $attribute) {
                $nestedData['id'] = $attribute->id;
                $nestedData['name'] = $attribute->name;
                $nestedData['key'] = $attribute->key;
                $nestedData['type'] = $attribute->type;
                $nestedData['actions'] = '<a class="btn btn-xs btn-success" href="' . route('admin.attribute.show',['attribute' => $attribute->id]) . '"> <i class="fa fa-eye"></i></a>
                                            <a class="btn btn-xs btn-warning" href="' . route('admin.attribute.edit',['attribute' => $attribute->id]) . '"> <i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-danger" href="#destroyModal" data-toggle="modal" data-url="' . route('admin.attribute.destroy',['attribute' => $attribute->id]) . '" data-text="' . trans('common.destroy_attribute', ['id' => $attribute->id]) . '"> <i class="fa fa-trash-o"></i></a>';

                $data[] = $nestedData;
            }
        }


        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
