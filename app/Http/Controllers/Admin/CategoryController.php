<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class CategoryController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        // $this->middleware('user.is.admin');

        View::share('title', trans('admin_category.index'));

        $this->breadcrumbs['admin.category.index'] = trans('admin_category.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categories = Category::pluck('name', 'id');

        return view('admin.pages.category.create', compact('parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        Category::create($request->all());

        return redirect()->route('admin.category.index')
            ->with('success', trans('admin_category.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.pages.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parent_categories = Category::pluck('name', 'id');

        return view('admin.pages.category.edit', compact('category', 'parent_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CategoryRequest  $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('admin.category.index')
            ->with('success', trans('admin_category.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin.category.index')
            ->with('success', trans('admin_category.success.delete'));
    }

    public function allCategories(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'updated_at',
            3 => 'actions',
        );

        $totalData = Category::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $categories = Category::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $categories =  Category::where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Category::where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = [];

        if(!empty($categories)) {
            foreach ($categories as $category) {
                $nestedData['id'] = $category->id;
                $nestedData['name'] = $category->name;
                $nestedData['updated_at'] = date('j M Y h:i a',strtotime($category->updated_at));
                $nestedData['actions'] = '<a class="btn btn-xs btn-success" href="' . route('admin.category.show',['category' => $category->id]) . '"> <i class="fa fa-eye"></i></a>
                                            <a class="btn btn-xs btn-warning" href="' . route('admin.category.edit',['category' => $category->id]) . '"> <i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-danger" href="#destroyModal" data-toggle="modal" data-url="' . route('admin.category.destroy',['category' => $category->id]) . '" data-text="' . trans('common.destroy_category', ['id' => $category->id]) . '"> <i class="fa fa-trash-o"></i></a>';

                $data[] = $nestedData;
            }
        }


        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
