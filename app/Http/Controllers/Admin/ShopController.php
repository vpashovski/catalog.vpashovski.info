<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Shop;
use App\Category;
use App\AttributeSet;
use App\Http\Requests\ShopRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class ShopController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('user.is.admin')->only('destroy');

        View::share('title', trans('admin_shop.index'));

        $this->breadcrumbs['admin.shop.index'] = trans('admin_shop.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::all();

        return view('admin.pages.shop.index', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attribute_sets = AttributeSet::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');

        return view('admin.pages.shop.create', compact('attribute_sets', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ShopRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopRequest $request)
    {
        $shop = Shop::create($request->all());
        $shop->images()->sync([$request->input('image_id')]);

        return redirect()->route('admin.shop.index')
            ->with('success', trans('admin_shop.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        return view('admin.pages.shop.show', compact('shop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop $shop)
    {
        $attribute_sets = AttributeSet::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');

        return view('admin.pages.shop.edit', compact('shop', 'attribute_sets', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ShopRequest  $request
     * @param  Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(ShopRequest $request, Shop $shop)
    {
        $shop->update($request->all());
        $shop->images()->sync([$request->input('image_id')]);

        return redirect()->route('admin.shop.index')
            ->with('success', trans('admin_shop.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        $shop->delete();

        return redirect()->route('admin.shop.index')
            ->with('success', trans('admin_shop.success.delete'));
    }
}
