<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class UserController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
         $this->middleware('user.is.admin');

        View::share('title', trans('admin_user.index'));

        $this->breadcrumbs['admin.user.index'] = trans('admin_user.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.pages.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Request\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User();
        $user->name     = $request->input('name');
        $user->email    = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->is_admin = $request->input('is_admin');
        $user->save();

        return redirect()->route('admin.user.index')
            ->with('success', trans('admin_user.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.pages.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.pages.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user->name     = $request->input('name');
        $user->email    = $request->input('email');
        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->is_admin = $request->input('is_admin');

        $user->save();

        return redirect()->route('admin.user.index')
            ->with('success', trans('admin_user.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('admin.user.index')
            ->with('success', trans('admin_user.success.delete'));
    }
}
