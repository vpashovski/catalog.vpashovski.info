<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Product;
use App\Attribute;

use App\Http\Controllers\AdminController as AdminController;

class ProductController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        // $this->middleware('user.is.admin');

        View::share('title', trans('admin_product.index'));

        $this->breadcrumbs['admin.product.index'] = trans('admin_product.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.pages.product.show', compact('product'));
    }

    public function allProducts(Request $request)
    {
        $columns = array(
            0 => 'products.id',
            1 => 'image',
            2 => 'attribute_value_varchar.value',
            3 => 'products.created_at',
            4 => 'products.updated_at',
            5 => 'show',
        );

        $totalData = Product::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $attribute = Attribute::where('key', 'like', 'name')->first();
        if(empty($request->input('search.value'))) {
            $products = Product::offset($start)
                ->join('attribute_value_varchar', 'products.id', '=', 'attribute_value_varchar.product_id')
                ->where('attribute_value_varchar.attribute_id', '=', $attribute->id)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->select('products.*')
                ->get();
        } else {
            $search = $request->input('search.value');

            $products =  Product::where('products.id', 'LIKE', "%{$search}%")
                ->join('attribute_value_varchar', 'products.id', '=', 'attribute_value_varchar.product_id')
                ->where('attribute_value_varchar.attribute_id', '=', $attribute->id)
                ->orWhere('attribute_value_varchar.value', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->select('products.*')
                ->get();

            $totalFiltered = Product::where('products.id', 'LIKE', "%{$search}%")
                ->join('attribute_value_varchar', 'products.id', '=', 'attribute_value_varchar.product_id')
                ->where('attribute_value_varchar.attribute_id', '=', $attribute->id)
                ->orWhere('attribute_value_varchar.value', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = [];

        if(!empty($products)) {
            foreach ($products as $product) {
                if ($product->image) {
                    $imgUrl = '<img src="' . $product->image->url . '" alt="Product Image" width="30" height="30">';
                } else {
                    $imgUrl = '<img src="'  . url('uploads/images/no-image.png') . '" alt="Product Image" width="30" height="30">';
                }

                $nestedData['id'] = $product->id;
                $nestedData['image'] = $imgUrl;
                $nestedData['name'] = $product->name;
                $nestedData['shop'] = '<a href="' . route('admin.shop.show', ['shop' => $product->shop]) . '">' . $product->shop->name . '</a>';
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($product->created_at));
                $nestedData['updated_at'] = date('j M Y h:i a',strtotime($product->updated_at));
                $nestedData['show'] = '<a class="btn btn-xs btn-success" href="' . route('admin.product.show',['product' => $product->id]) . '"> <i class="fa fa-eye"></i></a>';

                $data[] = $nestedData;
            }
        }


        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
