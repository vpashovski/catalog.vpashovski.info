<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Stat;
use View;
use App\Article;
use App\Product;
use App\Shop;
use App\Http\Controllers\AdminController;

class IndexController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        View::share('title', trans('admin_header.dashboard'));

        $this->breadcrumbs['admin.index'] = trans('admin_header.dashboard');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    public function index()
    {
        $chart_data = [];

        $chart_data['products'] = Stat::getChartData(Product::class);
        $chart_data['shops'] = Stat::getChartData(Shop::class);
        $chart_data['articles'] = Stat::getChartData(Article::class);

        $max_value = 0;
        foreach ($chart_data as $data) {
            foreach ($data as $key => $value) {
                if ($value > $max_value) {
                    $max_value = $value;
                }
            }
        }
        $max_value = (int)ceil($max_value / 5) * 5;

        // Product info
        $latest_products = Product::orderBy('created_at', 'desc')->limit(config('constant.dashboard_records'))->get();

        $most_visited_products = Product::join('stats', function ($join) {
            $join->on('products.id', '=', 'stats.statable_id')
                ->where('stats.statable_type', '=', Product::class);
        })
            ->orderBy('stats.views', 'desc')
            ->limit(config('constant.dashboard_records'))->get();

        // Shop info
        $latest_shops = Shop::orderBy('created_at', 'desc')->limit(config('constant.dashboard_records'))->get();

        $most_visited_shops = Shop::join('stats', function ($join) {
            $join->on('shops.id', '=', 'stats.statable_id')
                ->where('stats.statable_type', '=', Shop::class);
        })
            ->orderBy('stats.views', 'desc')
            ->limit(config('constant.dashboard_records'))->get();

        // Article info
        $latest_articles = Article::orderBy('created_at', 'desc')->limit(config('constant.dashboard_records'))->get();

        $most_visited_articles = Article::join('stats', function ($join) {
                $join->on('articles.id', '=', 'stats.statable_id')
                    ->where('stats.statable_type', '=', Article::class);
            })
            ->orderBy('stats.views', 'desc')
            ->limit(config('constant.dashboard_records'))->get();

        return view('admin.pages.index', compact(
            'chart_data',
            'max_value',
            'latest_products',
            'most_visited_products',
            'latest_shops',
            'most_visited_shops',
            'latest_articles',
            'most_visited_articles'
        ));
    }
}
