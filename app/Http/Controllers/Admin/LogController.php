<?php

namespace App\Http\Controllers\Admin;

use View;
use App\Log;

use App\Http\Controllers\AdminController as AdminController;

class LogController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        View::share('title', trans('admin_log.index'));

        $this->breadcrumbs['admin.log.index'] = trans('admin_log.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = Log::all();

        return view('admin.pages.log.index', compact('logs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Log $log
     * @return \Illuminate\Http\Response
     */
    public function destroy(Log $log)
    {
        $log->delete();

        return redirect()->route('admin.log.index')
            ->with('success', trans('admin_log.success.delete'));
    }
}
