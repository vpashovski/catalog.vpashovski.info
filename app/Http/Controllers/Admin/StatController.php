<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Stat;

use App\Http\Controllers\AdminController as AdminController;

class StatController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function products()
    {
        $title = trans('admin_stat.index_products');

        $items = Stat::getFilteredResults(\App\Product::class);

        return view('admin.pages.stat.index', compact('title', 'items'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shops()
    {
        $title = trans('admin_stat.index_shops');

        $items = Stat::getFilteredResults(\App\Shop::class);

        return view('admin.pages.stat.index', compact('title', 'items'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function articles()
    {
        $title = trans('admin_stat.index_articles');

        $items = Stat::getFilteredResults(\App\Article::class);

        return view('admin.pages.stat.index', compact('title', 'items'));
    }

}
