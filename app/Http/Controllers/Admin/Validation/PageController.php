<?php

namespace App\Http\Controllers\Admin\Validation;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function url(Request $request)
    {
        if (! $request->has('url')) {
            return response()->json(false);
        }

        $result = Page::where('url', '=', slugify($request->input('url')));
        if ($this->isEdit) {
            $result = $result->where('id', '!=', $this->entityId);
        }

        $result = $result->pluck('id');

        if (!$result->isEmpty()) {
            return response()->json(false);
        }

        return response()->json(true);
    }
}
