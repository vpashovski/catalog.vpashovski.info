<?php

namespace App\Http\Controllers\Admin\Validation;

use App\Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function key(Request $request)
    {
        if (! $request->has('key')) {
            return response()->json(false);
        }

        $result = Attribute::where('key', '=', $request->input('key'));
        if ($this->isEdit) {
            $result = $result->where('id', '!=', $this->entityId);
        }

        $result = $result->pluck('id');

        if (! $result->isEmpty()) {
            return response()->json(false);
        }

        return response()->json(true);
    }
}
