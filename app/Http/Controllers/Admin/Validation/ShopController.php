<?php

namespace App\Http\Controllers\Admin\Validation;

use App\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function slug(Request $request)
    {
        if (! $request->has('slug')) {
            return response()->json(false);
        }

        $result = Shop::where('slug', '=', slugify($request->input('slug')));
        if ($this->isEdit) {
            $result = $result->where('id', '!=', $this->entityId);
        }

        $result = $result->pluck('id');

        if (!$result->isEmpty()) {
            return response()->json(false);
        }

        return response()->json(true);
    }
}
