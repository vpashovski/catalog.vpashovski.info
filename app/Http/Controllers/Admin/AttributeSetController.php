<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Attribute;
use App\AttributeSet;
use App\Http\Requests\AttributeSetRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class AttributeSetController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();

        View::share('title', trans('admin_attribute_set.index'));

        $this->breadcrumbs['admin.attribute_set.index'] = trans('admin_attribute_set.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attribute_sets = AttributeSet::all();

        return view('admin.pages.attribute_set.index', compact('attribute_sets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributes = Attribute::pluck('name', 'id');

        return view('admin.pages.attribute_set.create', compact('attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\AttributeSetRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeSetRequest $request)
    {
        $attribute_set = AttributeSet::create($request->all());

        $this->syncAttributes($attribute_set, $request->input('attributes'));

        return redirect()->route('admin.attribute_set.index')
            ->with('success', trans('admin.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  AttributeSet  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function show(AttributeSet $attribute_set)
    {
        return view('admin.pages.attribute_set.show', compact('attribute_set'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  AttributeSet  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function edit(AttributeSet $attribute_set)
    {
        $attributes = Attribute::pluck('name', 'id');

        return view('admin.pages.attribute_set.edit', compact('attribute_set', 'attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\AttributeSetRequest  $request
     * @param  AttributeSet  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeSetRequest $request, AttributeSet $attribute_set)
    {
        $attribute_set->update($request->all());

        $this->syncAttributes($attribute_set, $request->input('attributes'));

        return redirect()->route('admin.attribute_set.index')
            ->with('success', trans('admin_attribute_set.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AttributeSet  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttributeSet $attribute_set)
    {
        $attribute_set->delete();

        return redirect()->route('admin.attribute_set.index')
            ->with('success', trans('admin_attibute_set.success.delete'));
    }

    protected function syncAttributes($attribute_set, $attributes)
    {
        $attributes = array_combine($attributes['id'], $attributes['xml_tag']);
        $attributes_sync = array();
        foreach ($attributes as $key => $value) {
            if (!$key || !$value) {
                continue;
            }
            $attributes_sync[$key] = array(
                'xml_tag'   => $value,
            );
        }

        $attribute_set->attributes()->sync($attributes_sync);
    }
}
