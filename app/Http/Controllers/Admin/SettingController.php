<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Setting;
use App\Http\Requests\SettingRequest;

use App\Http\Controllers\AdminController as AdminController;

class SettingController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('user.is.admin');

        View::share('title', trans('admin_setting.index'));

        $this->breadcrumbs['admin.setting.index'] = trans('admin_setting.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.setting.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Setting::saveSettings($request->all());

        return redirect()->route('admin.setting.index')
            ->with('success', trans('admin_setting.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
    }
}
