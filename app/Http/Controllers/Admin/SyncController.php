<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController as AdminController;

class SyncController extends AdminController
{

    public function index()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $result = \App\Sync::run(request()->get('store_id'));
        return response()->json([
            'result'        => $result,
        ]);
    }
}
