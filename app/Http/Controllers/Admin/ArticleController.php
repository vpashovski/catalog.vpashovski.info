<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Tag;
use App\User;
use App\Section;
use App\Article;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class ArticleController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        // $this->middleware('user.is.admin');

        View::share('title', trans('admin_article.index'));

        $this->breadcrumbs['admin.article.index'] = trans('admin_article.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.article.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::pluck('title', 'id');
        $tags = Tag::pluck('title', 'id')->toArray();

        return view('admin.pages.article.create', compact('sections', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ArticleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $article = Auth::user()->articles()->create($request->all());
        $this->syncTags($article, $request->input('tag_list'));
        $article->images()->sync([$request->input('image_id')]);

        return redirect()->route('admin.article.index')
            ->with('success', trans('admin_article.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('admin.pages.article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $sections = Section::pluck('title', 'id');
        $tags = Tag::pluck('title', 'id')->toArray();

        return view('admin.pages.article.edit', compact('article', 'sections', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ArticleRequest  $request
     * @param  Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->all());
        $this->syncTags($article, $request->input('tag_list'));
        $article->images()->sync([$request->input('image_id')]);

        return redirect()->route('admin.article.index')
            ->with('success', trans('admin_article.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return redirect()->route('admin.article.index')
            ->with('success', trans('admin_article.success.delete'));
    }

    private function syncTags(Article $article, $tag_list)
    {
        $tag_ids = [];
        if ($tag_list) {
            foreach ($tag_list as $value) {
                $tag = Tag::find($value);
                if (is_null($tag)) {
                    $tag = Tag::create([
                        'title' => $value,
                        'slug'  => slugify($value),
                    ]);
                }
                $tag_ids[] = $tag->id;
            }
        }

        $article->tags()->sync($tag_ids);
    }

    public function allArticles(Request $request)
    {
        $columns = array(
            0 => 'articles.id',
            1 => 'image',
            2 => 'articles.title',
            3 => 'articles.slug',
            4 => 'sections.title',
            5 => 'users.name',
            6 => 'articles.created_at',
            7 => 'articles.updated_at',
            8 => 'actions',
        );

        $totalData = Article::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $articles = Article::offset($start)
                ->join('sections', 'sections.id', '=', 'articles.section_id')
                ->join('users', 'users.id', '=', 'articles.user_id')
                ->limit($limit)
                ->orderBy($order, $dir)
                ->select('articles.*')
                ->get();

        } else {
            $search = $request->input('search.value');

            $articles = Article::where('articles.id', 'LIKE', "%{$search}%")
                ->join('sections', 'sections.id', '=', 'articles.section_id')
                ->join('users', 'users.id', '=', 'articles.user_id')
                ->orWhere('articles.title', 'LIKE', "%{$search}%")
                ->orWhere('articles.slug', 'LIKE', "%{$search}%")
                ->orWhere('sections.title', 'LIKE', "%{$search}%")
                ->orWhere('users.name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->select('articles.*')
                ->get();


            $totalFiltered = Article::where('articles.id', 'LIKE', "%{$search}%")
                ->join('sections', 'sections.id', '=', 'articles.section_id')
                ->join('users', 'users.id', '=', 'articles.user_id')
                ->orWhere('articles.title', 'LIKE', "%{$search}%")
                ->orWhere('articles.slug', 'LIKE', "%{$search}%")
                ->orWhere('sections.title', 'LIKE', "%{$search}%")
                ->orWhere('users.name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = [];

        if(!empty($articles)) {
            foreach ($articles as $article) {
                if ($article->image) {
                    $imgUrl = '<img src="' . $article->image->url . '" alt="Article Image" width="30" height="30">';
                } else {
                    $imgUrl = '<img src="'  . url('uploads/images/no-image.png') . '" alt="Article Image" width="30" height="30">';
                }

                if ($article->section) {
                    $section = '<a href="' . route('admin.section.edit', ['section' => $article->section->id]) . '"> ' . $article->section->title . '</a>';
                } else {
                    $section = '-';
                }

                $nestedData['id'] = $article->id;
                $nestedData['image'] = $imgUrl;
                $nestedData['title'] = $article->title;
                $nestedData['slug'] = $article->slug;
                $nestedData['section'] = $section;
                $nestedData['author'] = ($article->user) ? $article->user->name : '-';
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($article->created_at));
                $nestedData['updated_at'] = date('j M Y h:i a',strtotime($article->updated_at));
                $nestedData['actions'] = '<a class="btn btn-xs btn-success" href="' . route('admin.article.show',['article' => $article->id]) . '"> <i class="fa fa-eye"></i></a>
                                            <a class="btn btn-xs btn-warning" href="' . route('admin.article.edit',['article' => $article->id]) . '"> <i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-danger" href="#destroyModal" data-toggle="modal" data-url="' . route('admin.article.destroy',['article' => $article->id]) . '" data-text="' . trans('common.destroy_article', ['id' => $article->id]) . '"> <i class="fa fa-trash-o"></i></a>';

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
