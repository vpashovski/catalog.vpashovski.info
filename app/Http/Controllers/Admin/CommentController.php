<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Comment;
use App\Article;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\AdminController as AdminController;

class CommentController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        // $this->middleware('user.is.admin');

        View::share('title', trans('admin_comment.index'));

        $this->breadcrumbs['admin.comment.index'] = trans('admin_comment.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();

        return view('admin.pages.comment.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $articles = Article::pluck('title', 'id');

        return view('admin.pages.comment.create', compact('articles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $comment = Comment::create($request->all());
        $comment->checked = true;
        $comment->save();

        return redirect()->route('admin.comment.index')
            ->with('success', trans('admin_comment.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('admin.pages.comment.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        $articles = Article::pluck('title', 'id');

        return view('admin.pages.comment.edit', compact('comment', 'articles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CommentRequest  $request
     * @param  Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        $comment->update($request->all());
        $comment->checked = true;
        $comment->save();

        return redirect()->route('admin.comment.index')
            ->with('success', trans('admin_comment.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return redirect()->route('admin.comment.index')
            ->with('success', trans('admin_comment.success.delete'));
    }
}
