<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View;
use App\Page;
use App\Http\Requests\PageRequest;

use App\Http\Controllers\AdminController as AdminController;

class PageController extends AdminController
{
    protected $breadcrumbs = [];

    public function __construct()
    {
        parent::__construct();
        View::share('title', trans('admin_page.index'));

        $this->breadcrumbs['admin.page.index'] = trans('admin_page.index');

        if ($this->breadcrumbs) {
            View::share('breadcrumbs', $this->breadcrumbs);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();

        return view('admin.pages.page.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        Page::create($request->all());

        return redirect()->route('admin.page.index')
            ->with('success', trans('admin_page.success.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('admin.pages.page.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PageRequest  $request
     * @param  Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, Page $page)
    {
        $page->update($request->all());

        return redirect()->route('admin.page.index')
            ->with('success', trans('admin_page.success.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect()->route('admin.page.index')
            ->with('success', trans('admin_page.success.delete'));
    }
}
