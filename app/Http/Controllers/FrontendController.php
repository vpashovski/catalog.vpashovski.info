<?php

namespace App\Http\Controllers;

use View;
use App\Menu;
use App\Page;
use App\Article;
use App\Section;


class FrontendController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        \App::setLocale('bg');
        View::share('menus', Menu::order()->get());

        $footer_pages = Page::where('in_footer', 1)->limit(config('constant.footer_pages_limit'))->get();
        View::share('footer_pages', $footer_pages);

        if (request()->is('blog') || request()->is('blog/*')) {
            $sections = Section::order()->get();

            $recent_articles = Article::with(['section', 'images', 'user', 'tags'])
                ->ordered()->paginate(config('constant.recent_articles_limit'));

            View::share('right_sections', $sections);
            View::share('right_recent_articles', $recent_articles);
        }
    }
}
