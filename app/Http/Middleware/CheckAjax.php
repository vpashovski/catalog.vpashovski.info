<?php

namespace App\Http\Middleware;

use Closure;

class CheckAjax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->ajax()) {
            if (! $request->is('admin/*')) {
                return redirect()->route('index');
            } else {
                return redirect()->route('admin.index');
            }
        }

        return $next($request);
    }
}
