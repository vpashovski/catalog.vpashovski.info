<?php

namespace App;

use Request;

class Log extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'total_inserted_products',
        'total_updated_products',
        'total_deleted_products',
        'total_error_products',
    ];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
