<?php

namespace App;

use Request;

class Section extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description', 'order'
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = slugify($value);
    }
}
