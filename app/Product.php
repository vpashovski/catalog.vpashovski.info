<?php

namespace App;

use DB;
use Request;

class Product extends Model
{
    const NAME = 'name';

    const PRICE = 'price';

    const AFF_LINK = 'aff_link';

    const THUMB = 'thumb';

    const IMAGES = 'images';

    const CATEGORY = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id', 'slug', 'attribute_set_id', 'image_id'
    ];

    const ALL_ATTRIBUTES = '*';

    protected $types = ['int', 'varchar', 'text', 'decimal', 'datetime'];

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute_set()
    {
        return $this->belongsTo(AttributeSet::class);
    }

    public function attributeKeys()
    {
        $keys = [];
        foreach ($this->types as $type) {
            $result = DB::table("attribute_value_{$type}")->select('attribute_id')->where('product_id', '=', $this->id)->get();
            foreach ($result as $item) {
                $attribute = Attribute::find($item->attribute_id);
                $keys[$attribute->id] = $attribute->key;
            }
        }

        return $keys;
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function image()
    {
        return $this->hasOne(ProductImage::class, 'id', 'image_id');
    }

    public function images()
    {
        return $this->belongsToMany(ProductImage::class, 'image_product_image', 'image_id', 'product_id')->withTimestamps();
    }

    public function stats()
    {
        return $this->morphMany(Stat::class, 'statable');
    }

    public function getViewsAttribute()
    {
        $views = 0;
        foreach ($this->stats as $stat) {
            $views += $stat->views;
        }

        return $views;
    }

    public function getUniqueViewsAttribute()
    {
        return $this->stats()->count();
    }

    public function setData($key, $value)
    {
        $attribute = Attribute::where('key', 'like', $key)->first();
        if ($attribute) {
            DB::insert("INSERT INTO `attribute_value_{$attribute->type}` (`attribute_id`, `product_id`, `value`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = ?, updated_at = ?", [$attribute->id, $this->id, $value, \Carbon\Carbon::now(), \Carbon\Carbon::now(), $value, \Carbon\Carbon::now()]);
        }
    }

    public function getData($key, $names = false, $front_filter = true)
    {
        $return = null;
        if ($key != Attribute::ALL_ATTRIBUTES) {
            $attribute = Attribute::where('key', 'like', $key)->first();
            if ($attribute) {
                $result = DB::table("attribute_value_{$attribute->type}")->select('value')->where([
                    ['attribute_id', '=', $attribute->id],
                    ['product_id', '=', $this->id]
                ])->first();

                $return = $result ? $result->value : null;
            }
        } else {
            foreach ($this->types as $type) {
                $result = DB::table("attribute_value_{$type}")->select('value', 'attribute_id')->where('product_id', '=', $this->id)->get();

                foreach ($result as $item) {
                    $attribute = Attribute::find($item->attribute_id);
                    if (!$front_filter || $front_filter && $attribute->visible_in_front) {
                        if (!$names) {
                            $return[$attribute->key] = $item->value;
                        } else {
                            $return[$attribute->name] = $item->value;
                        }
                    }
                }
            }
        }

        return $return;
    }

    public function unsData($key)
    {
        if ($key != Attribute::ALL_ATTRIBUTES) {
            $attribute = Attribute::where('key', 'like', $key)->first();
            if ($attribute) {
                return DB::table("attribute_value_{$attribute->type}")->where([
                    ['attribute_id', '=', $attribute->id],
                    ['product_id', '=', $this->id]
                ])->delete();
            }
        } else {
            foreach ($this->types as $type) {
                DB::table("attribute_value_{$type}")->where('product_id', '=', $this->id)->delete();
            }
        }

        return true;
    }

    public function getNameAttribute($value)
    {
        return $this->getData('name');
    }
}
