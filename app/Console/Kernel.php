<?php

namespace App\Console;

use DB;
use App\Stat;
use App\Shop;
use App\Sync;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $shops = Shop::all();

        foreach ($shops as $shop) {
            $schedule->call(function() use($shop) {
                Sync::run($shop->id);
            })->cron($shop->cron_schedule);
        }

        $schedule->call(function () {
            DB::table('logs')->truncate();
        })->weekly();

        $schedule->call(function () {
            $stats = Stat::all();

            foreach($stats as $stat) {
                $class = new $stat->statable_type();
                $obj = $class::find($stat->statable_id);

                if (!$obj) {
                    $stat->delete();
                }
            }
        })->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
