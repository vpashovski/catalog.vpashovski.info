<?php

namespace App;

use Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public static function getFilteredResults()
    {
        $query = static::ordered();

        if (Request::has('id')) {
            $query = $query->where('id', Request::input('id'));
        }
        if (Request::has('name')) {
            $query = $query->where('name', 'like', '%' . Request::input('name') . '%');
        }
        if (Request::has('email')) {
            $query = $query->where('email', 'like', '%' . Request::input('email') . '%');
        }
        if (Request::has('is_admin')) {
            $query = $query->where('is_admin', Request::input('is_admin'));
        }

        return $query->paginate(config('constant.per_page'));
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('id', 'desc');
    }
}
