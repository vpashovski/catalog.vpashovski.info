<?php

namespace App;

use Request;
use App\Helpers\CronSchedule;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'feed_url', 'shop_url', 'shop_aff_url', 'attribute_set_id', 'category_id', 'cron_schedule', 'status'
    ];

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable')->withTimestamps();
    }

    public function getImageAttribute($value)
    {
        return $this->images()->first();
    }

    public function stats()
    {
        return $this->morphMany(Stat::class, 'statable');
    }

    public function getViewsAttribute()
    {
        $views = 0;
        foreach ($this->stats as $stat) {
            $views += $stat->views;
        }

        return $views;
    }

    public function getUniqueViewsAttribute()
    {
        return $this->stats()->count();
    }

    public function attribute_set()
    {
        return $this->belongsTo(AttributeSet::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function getHumanReadableCronScheduleAttribute($value)
    {
        $schedule = CronSchedule::fromCronString($this->cron_schedule);

        return $schedule->asNaturalLanguage();
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = slugify($value);
    }

    public function getRatingAttribute()
    {
        return ($this->rate_count) ? $this->rate / $this->rate_count : 0;
    }
}
