<?php

namespace App;

class ProductImage extends Model
{
    protected $fillable = ['title', 'ext'];

    public static $filepath = 'uploads/images';

    public static function getFilePath()
    {
        return public_path(static::$filepath);
    }

    public function getFileName()
    {
        return $this->getAttribute('title') . '.' . $this->getAttribute('ext');
    }

    public function getUrlAttribute()
    {
        return asset(static::$filepath . '/' . $this->getFileName());
    }

    public function getImagePathAttribute()
    {
        return static::getFilePath() . '/' . $this->getFileName();
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'image_product_image', 'image_id', 'product_id')->withTimestamps();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function getFilteredResults()
    {
        $query = static::ordered();

        if (Request::has('id')) {
            $query = $query->where('id', Request::input('id'));
        }
        if (Request::has('title')) {
            $query = $query->where('title', 'like','%' . Request::input('title') . '%');
        }

        return $query->paginate(config('constants.per_page'));
    }
}
