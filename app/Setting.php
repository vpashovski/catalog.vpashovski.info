<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'value', 'serialized'
    ];

    public static function get($key)
    {
        $result = null;

        $setting = Setting::where('key', '=', $key)->first();

        if ($setting) {
            if (!$setting->serialized) {
                $result = $setting->value;
            } else {
                $result = json_decode($setting->value, true);
            }
        }

        return $result;
    }

    public static function saveSettings($data)
    {
        DB::transaction(function () use ($data) {
            DB::table('settings')->truncate();

            foreach ($data as $key => $value) {
                if ($key != '_token') {
                    DB::insert(
                        "INSERT INTO `settings` (`key`, `value`, `serialized`) VALUES (?, ?, ?)",
                        [
                            $key,
                            is_array($value) ? json_encode($value) : $value,
                            is_array($value)
                        ]
                    );
                }
            }
        });
    }
}
