<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

/*
|--------------------------------------------------------------------------
| Login/Logout routes
|--------------------------------------------------------------------------
*/

Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
// Auth::routes();

/*
|--------------------------------------------------------------------------
| Front routes
|--------------------------------------------------------------------------
*/

// Route::get('/', ['as' => 'index', 'uses' => 'Front\IndexController@index']);
Route::get('',                                  ['as' => 'index',   'uses' => 'Frontend\IndexController@index']);
Route::get('category/{slug}/{sub_slug?}',       ['as' => 'category',   'uses' => 'Frontend\CategoryController@index']);
Route::get('product/{id}',                      ['as' => 'product',   'uses' => 'Frontend\ProductController@index']);
Route::get('page/{url}',                        ['as' => 'page',      'uses' => 'Frontend\PageController@index']);
Route::get('search',                            ['as' => 'search',    'uses' => 'Frontend\SearchController@index']);
Route::get('contact-us',                        ['as' => 'contact',   'uses' => 'Frontend\ContactController@index']);
Route::post('contact-us/send',                  ['as' => 'contact.send', 'uses' => 'Frontend\ContactController@send']);

Route::group(['prefix' => 'shop', 'as' => 'shop.'], function() {
    Route::get('/',                            ['as' => 'index',       'uses' => 'Frontend\ShopController@index']);
    Route::get('{shop_slug}',                  ['as' => 'view',        'uses' => 'Frontend\ShopController@view']);
    Route::get('{shop_slug}/products',         ['as' => 'products',    'uses' => 'Frontend\ShopController@products']);

    Route::group(['middleware' => 'ajax'], function() {
        Route::post('rate',     ['as' => 'rate',    'uses' => 'Frontend\ShopController@rate']);
    });
});


Route::group(['prefix' => 'blog', 'as' => 'blog.'], function() {
    Route::get('/',                             ['as' => 'index',   'uses' => 'Frontend\BlogController@index']);
    Route::get('tag/{tag_slug}',                ['as' => 'tag',     'uses' => 'Frontend\TagController@index']);
    Route::get('search',                        ['as' => 'search',  'uses' => 'Frontend\BlogController@search']);
    Route::get('{section_slug}',                ['as' => 'section', 'uses' => 'Frontend\SectionController@index']);
    Route::get('{section_slug}/{article_slug}', ['as' => 'article', 'uses' => 'Frontend\ArticleController@index']);

    Route::group(['middleware' => 'ajax'], function() {
        Route::post('comment',  ['as' => 'comment', 'uses' => 'Frontend\ArticleController@comment']);
        Route::post('rate',     ['as' => 'rate',    'uses' => 'Frontend\ArticleController@rate']);
    });
});

/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::get('index', ['as' => 'index', 'uses' => 'Admin\IndexController@index']);
    Route::resource('user', 'Admin\UserController');
    Route::resource('product', 'Admin\ProductController');
    Route::resource('category', 'Admin\CategoryController');
    Route::resource('sync', 'Admin\SyncController');
    Route::resource('shop', 'Admin\ShopController');
    Route::resource('image', 'Admin\ImageController');
    Route::resource('attribute_set', 'Admin\AttributeSetController');
    Route::resource('attribute', 'Admin\AttributeController');
    Route::resource('log', 'Admin\LogController');
    Route::resource('menu', 'Admin\MenuController');
    Route::resource('article', 'Admin\ArticleController');
    Route::resource('section', 'Admin\SectionController');
    Route::resource('comment', 'Admin\CommentController');
    Route::resource('tag', 'Admin\TagController');
    Route::resource('page', 'Admin\PageController');
    Route::resource('setting', 'Admin\SettingController');

    Route::group(['prefix' => 'stat', 'as' => 'stat.'], function() {
        Route::get('products', ['as' => 'products', 'uses' => 'Admin\StatController@products']);
        Route::get('shops',    ['as' => 'shops',    'uses' => 'Admin\StatController@shops']);
        Route::get('articles', ['as' => 'articles', 'uses' => 'Admin\StatController@articles']);
    });

    Route::post('menu/order', ['as' => 'menu.order', 'uses' => 'Admin\MenuController@order']);
    Route::post('section/order', ['as' => 'section.order', 'uses' => 'Admin\SectionController@order']);

    Route::post('product/allproducts', ['as' => 'product.allproducts', 'uses' => 'Admin\ProductController@allProducts']);
    Route::post('category/allcategories', ['as' => 'category.allcategories', 'uses' => 'Admin\CategoryController@allCategories']);
    Route::post('attribute/allattributes', ['as' => 'attribute.allattributes', 'uses' => 'Admin\AttributeController@allAttributes']);
    Route::post('article/allarticles', ['as' => 'article.allarticles', 'uses' => 'Admin\ArticleController@allArticles']);

    Route::group(['prefix' => 'validate', 'middleware' => 'ajax'], function() {
        Route::get('user/email', ['as' => 'validate.user.email', 'uses' => 'Admin\Validation\UserController@email']);
        Route::get('shop/slug',  ['as' => 'validate.shop.slug',  'uses' => 'Admin\Validation\ShopController@slug']);
        Route::get('attribute/key',  ['as' => 'validate.attribute.key',  'uses' => 'Admin\Validation\AttributeController@key']);
        Route::get('category/slug',  ['as' => 'validate.category.slug',  'uses' => 'Admin\Validation\CategoryController@slug']);
        Route::get('article/slug',  ['as' => 'validate.article.slug',  'uses' => 'Admin\Validation\ArticleController@slug']);
        Route::get('section/slug',  ['as' => 'validate.section.slug',  'uses' => 'Admin\Validation\SectionController@slug']);
        Route::get('tag/slug',  ['as' => 'validate.tag.slug',  'uses' => 'Admin\Validation\TagController@slug']);
        Route::get('page/url',  ['as' => 'validate.page.url',  'uses' => 'Admin\Validation\PageController@url']);
    });
});

/*
|--------------------------------------------------------------------------
| Redirect everything other back
|--------------------------------------------------------------------------
*/
Route::any('{any}', function() {
    return redirect()->back();
})->where('any', '.*');
