<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('feed_url');
            $table->string('shop_url');
            $table->string('shop_aff_url');
            $table->integer('attribute_set_id')->unsigned()->default(0);
            $table->integer('category_id')->unsigned()->default(0);
            $table->string('cron_schedule', 20);
            $table->float('rate')->default(0);
            $table->integer('rate_count')->unsigned()->default(0);
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('attribute_set_id')->references('id')->on('attribute_sets')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
