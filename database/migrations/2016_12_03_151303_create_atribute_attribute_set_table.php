<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtributeAttributeSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_attribute_set', function (Blueprint $table) {
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('attribute_set_id')->unsigned()->index();
            $table->string('xml_tag');
            $table->timestamps();

            $table->primary(['attribute_id', 'attribute_set_id']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('attribute_set_id')->references('id')->on('attribute_sets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_attribute_set');
    }
}
