<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_value_int', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->integer('value')->default(0);
            $table->timestamps();

            $table->unique(['attribute_id', 'product_id']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('attribute_value_varchar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->string('value')->nullable()->default(null);
            $table->timestamps();

            $table->unique(['attribute_id', 'product_id']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('attribute_value_decimal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->decimal('value', 12, 4)->default(0);
            $table->timestamps();

            $table->unique(['attribute_id', 'product_id']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('attribute_value_text', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->text('value');
            $table->timestamps();

            $table->unique(['attribute_id', 'product_id']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('attribute_value_datetime', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->dateTime('value');
            $table->timestamps();

            $table->unique(['attribute_id', 'product_id']);
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_value_int');
        Schema::dropIfExists('attribute_value_varchar');
        Schema::dropIfExists('attribute_value_decimal');
        Schema::dropIfExists('attribute_value_text');
        Schema::dropIfExists('attribute_value_datetime');
    }
}
