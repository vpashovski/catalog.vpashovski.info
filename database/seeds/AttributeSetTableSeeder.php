<?php

use App\AttributeSet;
use Illuminate\Database\Seeder;

class AttributeSetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AttributeSet::create([
            'name'  => 'Default'
        ]);
    }
}
