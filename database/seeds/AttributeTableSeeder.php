<?php

use App\Attribute;
use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attribute::create([
            'name'      => 'Name',
            'key'       => 'name',
            'type'      => 'varchar',
            'visible_in_front' => 1,
        ]);

        Attribute::create([
            'name'      => 'Price',
            'key'       => 'price',
            'type'      => 'decimal',
            'visible_in_front' => 0,
        ]);

        Attribute::create([
            'name'      => 'Description',
            'key'       => 'description',
            'type'      => 'text',
            'visible_in_front' => 0,
        ]);

        Attribute::create([
            'name'      => 'Thumbnail',
            'key'       => 'thumb',
            'type'      => 'int',
            'visible_in_front' => 0,
        ]);

        Attribute::create([
            'name'      => 'Images',
            'key'       => 'images',
            'type'      => 'int',
            'visible_in_front' => 0,
        ]);

        Attribute::create([
            'name'      => 'Affiliate link',
            'key'       => 'aff_link',
            'type'      => 'varchar',
            'visible_in_front' => 0,
        ]);

        Attribute::create([
            'name'      => 'Category',
            'key'       => 'category',
            'type'      => 'int',
            'visible_in_front' => 0,
        ]);
    }
}
