<?php

return [

    'navigation'            => 'Navigation',
    'dashboard'             => 'Dashboard',
    'users'                 => 'Users',
    'shops'                 => 'Shops',
    'attributes'            => 'Attributes',
    'attribute_sets'        => 'Attribute Sets',
    'catalog'               => 'Catalog',
    'products'              => 'Products',
    'categories'            => 'Categories',
    'logs'                  => 'Logs',
    'menus'                 => 'Menus',
    'blog'                  => 'Blog',
    'articles'              => 'Articles',
    'sections'              => 'Sections',
    'comments'              => 'Comments',
    'tags'                  => 'Tags',
    'pages'                 => 'Pages',
    'settings'              => 'Settings',
    'stats'                 => 'Statistics',

];
