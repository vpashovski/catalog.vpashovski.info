<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Pages',
    'create'    => 'Create Page',
    'edit'      => 'Edit Page',
    'show'      => 'Page details',
    'info'      => 'Info',
    'all'       => 'All Pages',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'title'         => 'Title',
        'url'           => 'Url',
        'in_footer'     => 'Show in footer',
        'updated_at'    => 'Updated at',
    ],
    'fields'     => [
        'title'         => 'Title',
        'url'           => 'Url',
        'content'       => 'Content',
        'in_footer'     => 'Show in footer',
    ],
    'success'       => [
        'create'        => 'Successfully added Page!',
        'update'        => 'Successfully updated Page!',
        'delete'        => 'Successfully deleted Page!',
    ],
];