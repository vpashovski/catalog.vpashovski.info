<?php

return [

    'login'     => 'Login',
    'email'     => 'Email',
    'password'  => 'Password',
    'remember'  => 'Remember Me',
    'signin'    => 'Sign In',

];
