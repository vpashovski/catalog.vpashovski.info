<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Statistics',
    'index_products' => 'Statistic for Products',
    'index_shops'    => 'Statistic for Shops',
    'index_articles' => 'Statistic for Articles',
    'products'  => 'Products',
    'shops'     => 'Shops',
    'articles'  => 'Articles',
    'filter'    => 'Filters',
    'order_by'  => 'Order By',
    'desc'      => 'DESC',
    'asc'       => 'ASC',
    'columns'   => [
        'id'            => '#',
        'unique_views'  => 'Unique views',
        'total_views'   => 'Total views',
        'last_visited'  => 'Last visited',
    ],
    'fields'     => [
        'name'          => 'Name',
        'slug'          => 'Slug',
        'feed_url'      => 'Feed URL',
        'shop_url'      => 'Shop URL',
        'shop_aff_url'  => 'Shop Affiliate URL',
        'attribute_set' => 'Attribute set',
        'category'      => 'Category',
        'cron_schedule' => 'Synchronize',
        'products_count' => 'Products count',
        'status'        => 'Status',
    ],
];