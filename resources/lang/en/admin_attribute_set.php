<?php

/*
|--------------------------------------------------------------------------
| Attribute Set Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'         => 'Attribute Sets',
    'create'        => 'Create Attribute Set',
    'edit'          => 'Edit Attribute Set',
    'show'          => 'Attribute Set',
    'info'          => 'Info',
    'all'           => 'All attribute sets',
    'id'            => 'ID',
    'filter'        => 'Filters',
    'attributes'    => 'Attributes',
    'xml_tags'      => 'XML Tags',
    'columns'       => [
        'id'            => '#',
        'name'          => 'Name',
    ],
    'fields'        => [
        'name'          => 'Name',
        'xml_product_tag'  => 'XML Product Tag',
    ],
    'success'       => [
        'create'        => 'Successfully added Attribute Set!',
        'update'        => 'Successfully updated Attribute Set!',
        'delete'        => 'Successfully deleted Attribute Set!',
    ],
];
