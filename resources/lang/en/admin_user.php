<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Users',
    'create'    => 'Create User',
    'edit'      => 'Edit User',
    'show'      => 'User Profile',
    'info'      => 'Info',
    'all'       => 'All users',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'name'          => 'Name',
        'email'         => 'Email',
        'is_admin'      => 'Is Admin',
    ],
    'fields'     => [
        'name'          => 'Name',
        'email'         => 'Email',
        'is_admin'      => 'Is Admin',
        'password'      => 'Password',
        'password_confirmation' => 'Confirm Password',
    ],
    'success'       => [
        'create'        => 'Successfully added User!',
        'update'        => 'Successfully updated User!',
        'delete'        => 'Successfully deleted User!',
    ],
];