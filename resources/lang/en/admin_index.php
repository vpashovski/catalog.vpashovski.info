<?php

/*
|--------------------------------------------------------------------------
| Attribute Language Lines
|--------------------------------------------------------------------------
*/
return [
    'weekly_details'            => 'Weekly Details',
    'products'                  => 'Products',
    'shops'                     => 'Shops',
    'articles'                  => 'Articles',
    'latest_products'           => 'Latest products',
    'most_visited_products'     => 'Most visited products',
    'latest_shops'              => 'Latest shops',
    'most_visited_shops'        => 'Most visited shops',
    'latest_articles'           => 'Latest articles',
    'most_visited_articles'     => 'Most visited articles',
];
