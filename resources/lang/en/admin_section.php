<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Sections',
    'create'    => 'Create Section',
    'edit'      => 'Edit Section',
    'show'      => 'Section details',
    'info'      => 'Info',
    'all'       => 'All Sections',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'                => '#',
        'title'             => 'Title',
        'updated_at'        => 'Updated at',
    ],
    'fields'     => [
        'title'             => 'Title',
        'slug'              => 'Slug',
        'description'       => 'Description',
        'order'             => 'Order',
    ],
    'success'       => [
        'create'            => 'Successfully added Section!',
        'update'            => 'Successfully updated Section!',
        'delete'            => 'Successfully deleted Section!',
    ],
];