<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Logs',
    'create'    => 'Create Log',
    'edit'      => 'Edit Log',
    'show'      => 'Log details',
    'info'      => 'Info',
    'all'       => 'All Logs',
    'id'        => 'ID',
    'error_status' => 'Warrning! Shop in disabled!',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'shop'                      => 'Shop',
        'total_inserted_products'   => 'Total inserted products',
        'total_updated_products'    => 'Total updated products',
        'total_deleted_products'    => 'Total deleted products',
        'total_error_products'      => 'Total error products',
        'created_at'                => 'Created at',
    ],
    'success'       => [
        'delete'        => 'Successfully deleted Log!',
    ],
];