<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Menus',
    'create'    => 'Create Menu',
    'edit'      => 'Edit Menu',
    'show'      => 'Menu details',
    'info'      => 'Info',
    'all'       => 'All Menus',
    'id'        => 'ID',
    'columns'   => [
        'id'            => '#',
        'title'         => 'Title',
        'url'           => 'URL',
        'updated_at'    => 'Updated at',
    ],
    'fields'     => [
        'title'         => 'Title',
        'url'           => 'URL',
        'order'         => 'Order',
    ],
    'success'       => [
        'create'        => 'Successfully added Menu!',
        'update'        => 'Successfully updated Menu!',
        'delete'        => 'Successfully deleted Menu!',
    ],
];