<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Categories',
    'create'    => 'Create Category',
    'edit'      => 'Edit Category',
    'show'      => 'Category details',
    'info'      => 'Info',
    'all'       => 'All Categories',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'                => '#',
        'name'              => 'Name',
        'updated_at'        => 'Updated at',
    ],
    'fields'     => [
        'name'              => 'Name',
        'slug'              => 'Slug',
        'parent_category'   => 'Parent category',
    ],
    'success'       => [
        'create'            => 'Successfully added Category!',
        'update'            => 'Successfully updated Category!',
        'delete'            => 'Successfully deleted Category!',
    ],
];