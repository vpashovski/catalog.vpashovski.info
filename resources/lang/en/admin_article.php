<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Articles',
    'create'    => 'Create Article',
    'edit'      => 'Edit Article',
    'show'      => 'Article details',
    'info'      => 'Info',
    'all'       => 'All Articles',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'image'         => 'Image',
        'title'         => 'Title',
        'slug'          => 'Slug',
        'section'       => 'Section',
        'author'        => 'Author',
        'created_at'    => 'Created at',
        'updated_at'    => 'Updated at',
    ],
    'fields'     => [
        'title'         => 'Title',
        'slug'          => 'Slug',
        'description'   => 'Description',
        'content'       => 'Content',
        'section'       => 'Section',
        'tags'          => 'Tags',
        'author'        => 'Author',
        'views'         => 'Views',
        'rating'        => 'Rating',
    ],
    'success'       => [
        'create'        => 'Successfully added Article!',
        'update'        => 'Successfully updated Article!',
        'delete'        => 'Successfully deleted Article!',
    ],
];