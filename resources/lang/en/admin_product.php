<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Products',
    'create'    => 'Create Product',
    'edit'      => 'Edit Product',
    'show'      => 'Product details',
    'info'      => 'Info',
    'all'       => 'All Products',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'image'         => 'Image',
        'name'          => 'Name',
        'shop'          => 'Shop',
        'created_at'    => 'Created at',
        'updated_at'    => 'Updated at',
        'status'        => 'Status',
    ],
    'fields'     => [
        'name'          => 'Name',
        'slug'          => 'Slug',
        'feed_url'      => 'Feed URL',
        'shop_url'      => 'Shop URL',
        'shop_aff_url'  => 'Shop Affiliate URL',
        'attribute_set' => 'Attribute set',
        'category'      => 'Category',
        'shop'          => 'Shop',
        'cron_schedule' => 'Synchronize',
        'products_count' => 'Products count',
        'status'        => 'Status',
    ],
    'success'       => [
        'create'        => 'Successfully added Product!',
        'update'        => 'Successfully updated Product!',
        'delete'        => 'Successfully deleted Product!',
    ],
];