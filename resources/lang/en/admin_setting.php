<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Settings',
    'edit'      => 'Edit Settings',
    'tabs'      => [
        'base'                      => 'Base',
        'contact'                   => 'Contact Info',
    ],
    'fields'     => [
        'site_name'                 => 'Site Name',
        'address'                   => 'Address',
        'site_description'          => 'Site Description',
        'frontend_color'            => 'Frontend Color',
        'about_us'                  => 'About us text',
        'contact_name'              => 'Contact name',
        'contact_address'           => 'Contact address',
        'full_contact_address'      => 'Full Contact address',
        'google_map'                => 'Google map',
        'contact_phone'             => 'Contact phone',
        'contact_email'             => 'Contact email',
        'facebook'                  => 'Facebook',
        'twitter'                   => 'Twitter',
        'linkedin'                  => 'Linkedin',
        'google_plus'               => 'Google +',
    ],
    'color_options' => [
        'blue'      => 'Blue',
        'brown'     => 'Brown',
        'green'     => 'Green',
        'orange'    => 'Orange',
        'red'       => 'Red',
        'yellow'    => 'Yellow',
    ],
    'success'       => [
        'update'        => 'Successfully updated Settings!',
    ],
];