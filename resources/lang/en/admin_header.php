<?php

return [

    'title'             => 'Online Catalog',
    'description'       => 'Store description',
    'home'              => 'Home',
    'dashboard'         => 'Dashboard',
    'products'          => 'Products',
    'categories'        => 'Categories',
    'shops'             => 'Shops',
    'comments'          => 'Comments',

];
