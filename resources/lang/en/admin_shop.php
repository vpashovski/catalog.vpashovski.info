<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Shops',
    'create'    => 'Create Shop',
    'edit'      => 'Edit Shop',
    'show'      => 'Shop details',
    'info'      => 'Info',
    'all'       => 'All Shops',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'image'         => 'Image',
        'name'          => 'Name',
        'cron_schedule' => 'Synchronize',
        'updated_at'    => 'Updated at',
        'status'        => 'Status',
    ],
    'fields'     => [
        'name'          => 'Name',
        'slug'          => 'Slug',
        'feed_url'      => 'Feed URL',
        'shop_url'      => 'Shop URL',
        'shop_aff_url'  => 'Shop Affiliate URL',
        'attribute_set' => 'Attribute set',
        'category'      => 'Category',
        'cron_schedule' => 'Synchronize',
        'rating'        => 'Raiting',
        'products_count' => 'Products count',
        'views'         => 'Views',
        'status'        => 'Status',
    ],
    'success'       => [
        'create'        => 'Successfully added Shop!',
        'update'        => 'Successfully updated Shop!',
        'delete'        => 'Successfully deleted Shop!',
    ],
];