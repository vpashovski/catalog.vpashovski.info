<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Comments',
    'create'    => 'Create Comment',
    'edit'      => 'Edit Comment',
    'show'      => 'Comment details',
    'info'      => 'Info',
    'all'       => 'All Comments',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'                => '#',
        'commentator_name'  => 'Commentator name',
        'article'           => 'Article',
        'approved'          => 'Approved',
        'comment'           => 'Comment',
        'updated_at'        => 'Updated at',
    ],
    'fields'     => [
        'commentator_name'   => 'Commentator name',
        'commentator_email'  => 'Commentator email',
        'comment'            => 'Comment',
        'approved'           => 'Approved',
        'article'            => 'Article',
    ],
    'success'       => [
        'create'        => 'Successfully added Comment!',
        'update'        => 'Successfully updated Comment!',
        'delete'        => 'Successfully deleted Comment!',
    ],
];