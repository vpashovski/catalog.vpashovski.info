<?php

/*
|--------------------------------------------------------------------------
| Attribute Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Attributes',
    'create'    => 'Create Attribute',
    'edit'      => 'Edit Attribute',
    'show'      => 'Attribute',
    'info'      => 'Info',
    'all'       => 'All attributes',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'name'          => 'Name',
        'type'          => 'Type',
        'key'           => 'Key',
    ],
    'fields'     => [
        'id'            => '#',
        'name'          => 'Name',
        'type'          => 'Type',
        'key'           => 'Key',
        'visible_in_front' => 'Visible in front',
    ],
    'success'       => [
        'create'        => 'Successfully added Attribute!',
        'update'        => 'Successfully updated Attribute!',
        'delete'        => 'Successfully deleted Attribute!',
    ],
];
