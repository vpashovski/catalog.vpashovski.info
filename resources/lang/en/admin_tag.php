<?php

/*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
*/
return [
    'index'     => 'Tags',
    'create'    => 'Create Tag',
    'edit'      => 'Edit Tag',
    'show'      => 'Tag details',
    'info'      => 'Info',
    'all'       => 'All Tags',
    'id'        => 'ID',
    'filter'    => 'Filters',
    'columns'   => [
        'id'            => '#',
        'title'         => 'Title',
        'updated_at'    => 'Updated at',
    ],
    'fields'     => [
        'title'         => 'Title',
        'slug'          => 'Slug',
    ],
    'success'       => [
        'create'        => 'Successfully added Tag!',
        'update'        => 'Successfully updated Tag!',
        'delete'        => 'Successfully deleted Tag!',
    ],
];