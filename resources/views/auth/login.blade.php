<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.partials.head')
    </head>

    <body>
        <div class="admin-form">
          <div class="container">

            <div class="row">
              <div class="col-md-12">
                <!-- Widget starts -->
                    <div class="widget worange">
                      <!-- Widget head -->
                      <div class="widget-head">
                        <i class="fa fa-lock"></i> {{ trans('login.login') }} 
                      </div>

                      <div class="widget-content">
                        <div class="padd">
                          <!-- Login form -->
                          <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <!-- Email -->
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label class="control-label col-lg-3" for="email">{{ trans('login.email') }}</label>
                              <div class="col-lg-9">
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="{{ trans('login.email') }}" required autofocus>
                                @if ($errors->has('email'))
                                  <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                   </span>
                                @endif
                              </div>
                            </div>
                            <!-- Password -->
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label class="control-label col-lg-3" for="password">{{ trans('login.password') }}</label>
                              <div class="col-lg-9">
                                <input type="password" class="form-control" name="password" id="password" placeholder="{{ trans('login.password') }}" required>
                                @if ($errors->has('password'))
                                  <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                                @endif
                              </div>
                            </div>
                            <!-- Remember me checkbox and sign in button -->
                            <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                              <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name="remember"> {{ trans('login.remember') }}
                                  </label>
                              </div>
                            </div>
                            </div>
                                <div class="col-lg-9 col-lg-offset-3">
                                    <button type="submit" class="btn btn-info btn-sm">{{ trans('login.signin') }}</button>
                                </div>
                            <br />
                          </form>
                        </div>
                        </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
        {!! Html::script('js/admin/jquery.js') !!} <!-- jQuery -->
        {!! Html::script('js/admin/bootstrap.min.js') !!} <!-- Bootstrap -->
        {!! Html::script('js/app.js') !!} <!-- Bootstrap -->
    </body>
</html>
