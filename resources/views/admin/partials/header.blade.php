<header>
  <div class="container">
    <div class="row">
      <!-- Logo section -->
      <div class="col-md-4">
        <!-- Logo. -->
        <div class="logo">
          <h1>
            <a href="{{ route('admin.index') }}">
              @if(Setting::get('site_name'))
                {{ Setting::get('site_name') }}
              @else
                {{ config('app.name', trans('admin_header.title')) }}
              @endif
            </a>
          </h1>
          <p class="meta">
            @if(Setting::get('site_description'))
              {{ Setting::get('site_description') }}
            @else
              {{ trans('admin_header.description') }}
            @endif
          </p>
        </div>
        <!-- Logo ends -->
      </div>

      <!-- Button section -->
      <div class="col-md-4">

        <!-- Buttons -->
        <ul class="nav nav-pills">
          <!-- Comment button with number of latest comments count -->
          <li class="pull-right">
            <a href="{{ route('admin.comment.index') }}">
              <i class="fa fa-comments"></i> {{ trans('admin_header.comments') }} <span class="label label-info">{{ $unchecked_comments_count }}</span>
            </a>
          </li>
        </ul>
      </div>
      <!-- Data section -->
      <div class="col-md-4">
        <div class="header-data">

          <div class="hdata">
            <div class="mcol-left">
              <i class="fa fa-shopping-cart bgreen"></i>
            </div>
            <div class="mcol-right">
              <p><a href="#">{{ $shops_count }}</a><em>{{ trans('admin_header.shops') }}</em></p>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="hdata">
            <div class="mcol-left">
              <i class="fa fa-tags bblue"></i>
            </div>
            <div class="mcol-right">
              <p><a href="#">{{ $products_count }}</a> <em>{{ trans('admin_header.products') }}</em></p>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="hdata">
            <div class="mcol-left">
              <i class="fa fa-tags bred"></i>
            </div>
            <div class="mcol-right">
              <p><a href="#">{{ $categories_count }}</a> <em>{{ trans('admin_header.categories') }}</em></p>
            </div>
            <div class="clearfix"></div>
          </div>

        </div>
      </div>
    </div>
  </div>
</header>
