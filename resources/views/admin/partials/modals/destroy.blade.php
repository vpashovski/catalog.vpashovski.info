<div id="destroyModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">{{ trans('common.destroy') }}</h4>
          </div>
          <div class="modal-body">
            <p id="modal-body"></p>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" type="button" class="btn btn-default" aria-hidden="true">
                {{ trans('common.cancel') }}
            </button>
            <button type="button" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('destroy-form').submit();">{{ trans('common.destroy') }}</button>
            {!! Form::open(['id' => 'destroy-form', 'method' => 'DELETE']) !!}
            {!! Form::close() !!}
          </div>
      </div>
    </div>
</div>