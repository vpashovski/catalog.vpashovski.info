  <div class="page-head">
    <h2 class="pull-left"><i class="fa fa-bookmark"></i> {{ $title }}</h2>
    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
      <a href="{{ route('admin.index') }}"><i class="fa fa-home"></i> {{ trans('admin_header.home') }}</a> 
      <!-- Divider -->
      @if (isset($breadcrumbs))
        @foreach ($breadcrumbs as $k => $v)
            <span class="divider">/</span> 
          <a href='{{ route("{$k}") }}' class="bread-current">{{ $v }}</a>
        @endforeach
      @endif

    </div>
    <div class="clearfix"></div>
  </div>