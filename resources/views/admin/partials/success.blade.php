@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <div><i class="icon fa fa-check"></i> {{ session('success') }}</div>
    </div>
@endif