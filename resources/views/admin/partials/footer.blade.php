<!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; {{ \Carbon\Carbon::now()->format('Y') }} | <a href="#">Online Catalog</a> </p>
      </div>
    </div>
  </div>
</footer>

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 

{!! Html::script('js/admin/respond.min.js') !!}
<!--[if lt IE 9]>
{!! Html::script('js/admin/html5shiv.js') !!}
<![endif]-->

<!-- JS -->
{!! Html::script('js/jquery.js') !!} <!-- jQuery -->
{!! Html::script('js/bootstrap.min.js') !!} <!-- Bootstrap -->
{!! Html::script('js/jquery-ui.min.js') !!} <!-- jQuery UI -->
{!! Html::script('js/admin/moment.min.js') !!} <!-- Moment js for full calendar -->
{!! Html::script('js/admin/fullcalendar.min.js') !!} <!-- Full Google Calendar - Calendar -->
{!! Html::script('js/jquery.rateit.min.js') !!} <!-- RateIt - Star rating -->
{!! Html::script('js/admin/jquery.prettyPhoto.js') !!} <!-- prettyPhoto -->
{!! Html::script('js/admin/jquery.slimscroll.min.js') !!} <!-- jQuery Slim Scroll -->
{!! Html::script('js/admin/jquery.dataTables.min.js') !!} <!-- Data tables -->

<!-- jQuery Flot -->
{!! Html::script('js/admin/excanvas.min.js') !!}
{!! Html::script('js/admin/jquery.flot.js') !!}
{!! Html::script('js/admin/jquery.flot.resize.js') !!}
{!! Html::script('js/admin/jquery.flot.pie.js') !!}
{!! Html::script('js/admin/jquery.flot.stack.js') !!}
{!! Html::script('js/admin/jquery.flot.time.js') !!}

<!-- jQuery Notification - Noty -->
{!! Html::script('js/admin/jquery.noty.js') !!} <!-- jQuery Notify -->
{!! Html::script('js/admin/themes/default.js') !!} <!-- jQuery Notify -->
{!! Html::script('js/admin/layouts/bottom.js') !!} <!-- jQuery Notify -->
{!! Html::script('js/admin/layouts/topRight.js') !!} <!-- jQuery Notify -->
{!! Html::script('js/admin/layouts/top.js') !!} <!-- jQuery Notify -->
<!-- jQuery Notification ends -->

{!! Html::script('js/admin/sparklines.js') !!} <!-- Sparklines -->
{{--{!! Html::script('js/admin/jquery.cleditor.min.js') !!} <!-- CLEditor -->--}}
{!! Html::script('js/admin/bootstrap-datetimepicker.min.js') !!} <!-- Date picker -->
{!! Html::script('js/admin/jquery.onoff.min.js') !!} <!-- Bootstrap Toggle -->
{!! Html::script('js/admin/filter.js') !!} <!-- Filter for support page -->
{!! Html::script('js/admin/custom.js') !!} <!-- Custom codes -->