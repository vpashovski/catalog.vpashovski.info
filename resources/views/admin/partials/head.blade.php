<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- Title and other stuffs -->
<title>{{ isset($title) ? $title : config('app.name', 'Online Catalog') }} | Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<!-- Stylesheets -->
{!! Html::style('css/bootstrap.min.css') !!}
<!-- Font awesome icon -->
 {!! Html::style('css/font-awesome.min.css') !!}
{!! Html::style('css/font-awesome-animation.min.css') !!}

<!-- jQuery UI -->
{!! Html::style('css/font-awesome.css') !!}
<!-- Calendar -->
{!! Html::style('css/admin/fullcalendar.css') !!}
<!-- prettyPhoto -->
{!! Html::style('css/admin/prettyPhoto.css') !!}
<!-- Star rating -->
{!! Html::style('css/admin/rateit.css') !!}
<!-- Date picker -->
{!! Html::style('css/admin/bootstrap-datetimepicker.min.css') !!}
<!-- CLEditor -->
{!! Html::style('css/admin/jquery.cleditor.css') !!}
<!-- Data tables -->
{!! Html::style('css/admin/jquery.dataTables.css') !!}
<!-- Bootstrap toggle -->
{!! Html::style('css/admin/jquery.onoff.css') !!}
<!-- Main stylesheet -->
{!! Html::style('css/admin/style.css') !!}
<!-- Widgets stylesheet -->
{!! Html::style('css/admin/widgets.css') !!}

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon/favicon.png">

@yield('head_styles')