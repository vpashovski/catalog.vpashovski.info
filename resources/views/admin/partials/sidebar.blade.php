<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">{{ trans('admin_sidebar.navigation') }}</a></div>

    <!--- Sidebar navigation -->
    <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
    <ul id="nav">
      <!-- Main menu with font awesome icon -->
      <li @if(Request::segment(2) == 'index') class="open" @endif>
        <a href="{{ route('admin.index') }}"><i class="fa fa-home"></i> {{ trans('admin_sidebar.dashboard') }}</a>
      </li>
      <li @if(Request::segment(2) == 'user') class="open" @endif>
        <a href="{{ route('admin.user.index') }}"><i class="fa fa-user"></i> {{ trans('admin_sidebar.users') }}</a>
      </li>
      <li class="has_sub @if(in_array(Request::segment(2), ['product', 'category', 'log'])) open @endif" >
          <a href="#"><i class="fa fa-tags"></i> {{ trans('admin_sidebar.catalog') }}</a>
          <ul>
              <li @if(Request::segment(2) == 'product') class="current" @endif>
                  <a href="{{ route('admin.product.index') }}"><i class="fa fa-tags"></i> {{ trans('admin_sidebar.products') }}</a>
              </li>
              <li @if(Request::segment(2) == 'category') class="current" @endif>
                  <a href="{{ route('admin.category.index') }}"><i class="fa fa-tag"></i> {{ trans('admin_sidebar.categories') }}</a>
              </li>
              <li @if(Request::segment(2) == 'log') class="current" @endif>
                  <a href="{{ route('admin.log.index') }}"><i class="fa fa-clock-o"></i> {{ trans('admin_sidebar.logs') }}</a>
              </li>
          </ul>
      </li>
      <li @if(Request::segment(2) == 'shop') class="open" @endif>
        <a href="{{ route('admin.shop.index') }}"><i class="fa fa-shopping-cart"></i> {{ trans('admin_sidebar.shops') }}</a>
      </li>
      <li class="has_sub @if(Request::segment(2) == 'attribute_set' || Request::segment(2) == 'attribute') open @endif" >
        <a href="#"><i class="fa fa-cubes"></i> {{ trans('admin_sidebar.attributes') }}</a>
        <ul>
            <li @if(Request::segment(2) == 'attribute') class="current" @endif>
              <a href="{{ route('admin.attribute.index') }}"><i class="fa fa-cubes"></i> {{ trans('admin_sidebar.attributes') }}</a>
            </li>
            <li @if(Request::segment(2) == 'attribute_set') class="current" @endif>
              <a href="{{ route('admin.attribute_set.index') }}"><i class="fa fa-cube"></i> {{ trans('admin_sidebar.attribute_sets') }}</a>
            </li>
        </ul>
      </li>
      <li class="has_sub @if(in_array(Request::segment(2), ['article', 'section', 'comment', 'tag'])) open @endif" >
          <a href="#"><i class="fa fa-newspaper-o"></i> {{ trans('admin_sidebar.blog') }}</a>
          <ul>
              <li @if(Request::segment(2) == 'article') class="current" @endif>
                  <a href="{{ route('admin.article.index') }}"><i class="fa fa-newspaper-o"></i> {{ trans('admin_sidebar.articles') }}</a>
              </li>
              <li @if(Request::segment(2) == 'section') class="current" @endif>
                  <a href="{{ route('admin.section.index') }}"><i class="fa fa-object-group"></i> {{ trans('admin_sidebar.sections') }}</a>
              </li>
              <li @if(Request::segment(2) == 'comment') class="current" @endif>
                  <a href="{{ route('admin.comment.index') }}"><i class="fa fa-comments-o"></i> {{ trans('admin_sidebar.comments') }}</a>
              </li>
              <li @if(Request::segment(2) == 'tag') class="current" @endif>
                  <a href="{{ route('admin.tag.index') }}"><i class="fa fa-tags"></i> {{ trans('admin_sidebar.tags') }}</a>
              </li>
          </ul>
      </li>
      <li @if(Request::segment(2) == 'page') class="open" @endif>
          <a href="{{ route('admin.page.index') }}"><i class="fa fa-file"></i> {{ trans('admin_sidebar.pages') }}</a>
      </li>
      <li @if(Request::segment(2) == 'menu') class="open" @endif>
          <a href="{{ route('admin.menu.index') }}"><i class="fa fa-list"></i> {{ trans('admin_sidebar.menus') }}</a>
      </li>
      <li @if(Request::segment(2) == 'setting') class="open" @endif>
          <a href="{{ route('admin.setting.index') }}"><i class="fa fa-gears"></i> {{ trans('admin_sidebar.settings') }}</a>
      </li>
      <li class="has_sub @if(in_array(Request::segment(2), ['stat'])) open @endif" >
          <a href="#"><i class="fa fa-clock-o"></i> {{ trans('admin_sidebar.stats') }}</a>
          <ul>
              <li @if(Request::segment(3) == 'products') class="current" @endif>
                  <a href="{{ route('admin.stat.products') }}"><i class="fa fa-tags"></i> {{ trans('admin_sidebar.products') }}</a>
              </li>
              <li @if(Request::segment(3) == 'shops') class="current" @endif>
                  <a href="{{ route('admin.stat.shops') }}"><i class="fa fa-shopping-cart"></i> {{ trans('admin_sidebar.shops') }}</a>
              </li>
              <li @if(Request::segment(3) == 'articles') class="current" @endif>
                  <a href="{{ route('admin.stat.articles') }}"><i class="fa fa-newspaper-o"></i> {{ trans('admin_sidebar.articles') }}</a>
              </li>
          </ul>
      </li>
    </ul>
</div>
