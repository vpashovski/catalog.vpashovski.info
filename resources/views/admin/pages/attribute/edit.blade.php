@extends('layouts.admin')

@section('content')
    <div class="row">
        {!! Form::model($attribute, ['method' => 'PATCH', 'files' => true,
            'url' => route('admin.attribute.update', ['attribute' => $attribute]),
            'class' => 'attribute-form']) !!}
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_attribute.create') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        <div class="form">
                       <!-- form start -->
                        @include('admin.pages.attribute._form')
                        <!-- form end -->
                        </div>
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
