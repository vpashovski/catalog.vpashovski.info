@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_attribute.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.attribute.create') }}">
                    {{ trans('admin_attribute.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                    <table id="attributes" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                            <th>{{ trans('admin_attribute.columns.id') }}</th>
                            <th>{{ trans('admin_attribute.columns.name') }}</th>
                            <th>{{ trans('admin_attribute.columns.key') }}</th>
                            <th>{{ trans('admin_attribute.columns.type') }}</th>
                            <th></th>
                        </tr>
                      </thead>
                    </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#attributes').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }],
                "ajax":{
                    "url": "{{ route('admin.attribute.allattributes') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "processing": true,
                "serverSide": true,
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "key"},
                    {"data": "type"},
                    {"data": "actions"}
                ],
            });
        });
    </script>
@endsection
