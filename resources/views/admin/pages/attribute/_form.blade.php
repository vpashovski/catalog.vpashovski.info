<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', trans('admin_attribute.fields.name')) !!}
        {!! Form::text('name', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_attribute.fields.name')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('key', trans('admin_attribute.fields.key')) !!}
        {!! Form::text('key', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_attribute.fields.key')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('type', trans('admin_attribute.fields.type')) !!}
        {!! Form::select('type', $types, null,
        ['class' => 'form-control',  'required' => 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('visible_in_front', trans('admin_attribute.fields.visible_in_front')) !!}
        {!! Form::select('visible_in_front', [0 => trans('common.no'), 1 => trans('common.yes')], null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.attribute.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}

    <script>
        jQuery(function($) {
            @if (isset($attribute))
                $('#key').attr('disabled', true);
                $('#type').attr('disabled', true);

                $('.attribute-form').submit(function() {
                    $('#key').attr('disabled', false);
                    $('#type').attr('disabled', false);
                    return true;
                });
            @endif
            $('.attribute-form').validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 255
                    },
                    key: {
                        required: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.attribute.key') }}'
                    },
                    type: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $('.select2').select2();
        });
    </script>
@endsection
