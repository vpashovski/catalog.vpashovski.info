@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_attribute.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_attribute.fields.id') !!}</th>
                                <td>{{ $attribute->id }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_attribute.fields.name') !!}</th>
                                <td>{{ $attribute->name }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_attribute.fields.key') !!}</th>
                                <td>{{ $attribute->key }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_attribute.fields.type') !!}</th>
                                <td>{{ $attribute->type }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.attribute.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.attribute.edit', ['attribute' => $attribute->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_attribute.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
