@extends('layouts.admin')

@section('content')
    <div class="row">
      {!! Form::open(['method' => 'POST', 'files' => true,
          'url' => route('admin.article.store'),
          'class' => 'article-form']) !!}
        <div class="col-md-8">
          <div class="widget">
              <div class="widget-head">
                <div class="pull-left">{{ trans('admin_article.create') }}</div>
                <div class="widget-icons pull-right">
                  <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="widget-content">
                  <div class="padd">
                      <div class="form">
                        <!-- Edit profile form (not working)-->
                        <!-- form start -->
                        @include('admin.pages.article._form')
                        <!-- form end -->
                      </div>
                  </div>
                  <div class="widget-foot">
                  <!-- Footer goes here -->
                  </div>
              </div>
          </div>
        </div>
        @include('admin.partials.right_form_image', isset($article) ? ['record' => $article] : [])
      {!! Form::close() !!}

      @include('admin.partials.modals.select_image')
      @include('admin.partials.modals.upload_image')
    </div>
@endsection