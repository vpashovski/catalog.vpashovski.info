@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_article.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_article.fields.title') !!}</th>
                                <td>{{ $article->title }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.slug') !!}</th>
                                <td>{{ $article->slug }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.description') !!}</th>
                                <td>{!! $article->description !!}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.content') !!}</th>
                                <td>{!! $article->content !!}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.views') !!}</th>
                                <td>{{ $article->views }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.rating') !!}</th>
                                <td><div class="rateit" data-rateit-value="{{ $article->rating }}" data-rateit-readonly="true" data-rateit-resetable="false"></div></td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.section') !!}</th>
                                <td>
                                    @if($article->section)
                                        <a href="{{ route('admin.section.edit',
                                        ['section' => $article->section->id]) }}">
                                            {{ $article->section->title }}
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_article.fields.author') !!}</th>
                                <td>
                                    @if($article->user)
                                        {{ $article->user->name }}
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.article.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.article.edit', ['article' => $article->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_article.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('common.image') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        @if($article->image)
                            {!! Html::image($article->image->url, 'selected image') !!}
                        @else
                            {!! Html::image('assets/img/no-image.png', 'selected image') !!}
                        @endif
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_comment.index') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('admin_comment.columns.id') }}</th>
                                <th>{{ trans('admin_comment.columns.commentator_name') }}</th>
                                <th>{{ trans('admin_comment.columns.comment') }}</th>
                                <th>{{ trans('admin_comment.columns.approved') }}</th>
                                <th>{{ trans('admin_comment.columns.updated_at') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($article->comments as $comment)
                                <tr>
                                    <td>
                                        {{ $comment->id }} @if(!$comment->checked) <span class="label label-info">{{ trans('common.new') }}</span> @endif</td>
                                    <td>{{ $comment->commentator_name }} @if($comment->commentator_email) ({{ $comment->commentator_email }}) @endif</td>
                                    <td>{{ $comment->comment }}</td>
                                    <td>
                                        @if ($comment->approved)
                                            <span class="label label-success"><i class="fa fa-check"></i></span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-close"></i></span>
                                        @endif
                                    </td>
                                    <td>{{ $comment->updated_at }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-warning"
                                           href="{{ route('admin.comment.edit',
                                    ['comment' => $comment->id]) }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
