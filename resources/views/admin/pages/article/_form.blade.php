<div class="box-body">
    <div class="form-group">
        {!! Form::label('title', trans('admin_article.fields.title')) !!}
        {!! Form::text('title', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_article.fields.title')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', trans('admin_article.fields.slug')) !!}
        {!! Form::text('slug', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_article.fields.slug')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('description', trans('admin_article.fields.description')) !!}
        {!! Form::textarea('description', null,
        ['class' => 'form-control', 'rows' => '5',
        'placeholder' => trans('admin_article.fields.description')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('content', trans('admin_article.fields.content')) !!}
        {!! Form::textarea('content', null,
        ['class' => 'form-control', 'rows' => '10',
        'required' => 'required',
        'placeholder' => trans('admin_article.fields.content')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('section_id', trans('admin_article.fields.section')) !!}
        {!! Form::select('section_id', $sections, null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('tag_list', trans('admin_article.fields.tags')) !!}
        {!! Form::select('tag_list[]', $tags, null,
        ['id' => 'tags', 'class' => 'form-control select2', 'multiple']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.article.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/admin/ckeditor/ckeditor.js') !!}
    {!! Html::script('js/admin/images.js') !!}
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    <script>
        jQuery(function($) {
            $('.article-form').validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 255
                    },
                    slug: {
                        required: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.article.slug') }}'
                    },
                     section_id: {
                         required: true
                     }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        $(".select2").select2({
            placeholder: '{!! trans('admin_article.fields.tags') !!}',
            tags: true,
        })

        CKEDITOR.replace('description');
        CKEDITOR.replace('content');
    </script>
@endsection