@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_article.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.article.create') }}">
                    {{ trans('admin_article.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                    <table id="articles" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                            <th>{{ trans('admin_article.columns.id') }}</th>
                            <th>{{ trans('admin_article.columns.image') }}</th>
                            <th>{{ trans('admin_article.columns.title') }}</th>
                            <th>{{ trans('admin_article.columns.slug') }}</th>
                            <th>{{ trans('admin_article.columns.section') }}</th>
                            <th>{{ trans('admin_article.columns.author') }}</th>
                            <th>{{ trans('admin_article.columns.created_at') }}</th>
                            <th>{{ trans('admin_article.columns.updated_at') }}</th>
                            <th></th>
                        </tr>
                      </thead>
                    </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>


    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#articles').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: [1, 8]
                }],
                "ajax":{
                    "url": "{{ route('admin.article.allarticles') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "processing": true,
                "serverSide": true,
                "columns": [
                    {"data": "id"},
                    {"data": "image"},
                    {"data": "title"},
                    {"data": "slug"},
                    {"data": "section"},
                    {"data": "author"},
                    {"data": "created_at"},
                    {"data": "updated_at"},
                    {"data": "actions"}
                ],
            });
        });
    </script>
@endsection