@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_section.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.section.create') }}">
                    {{ trans('admin_section.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                        <table id="sections" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_section.columns.id') }}</th>
                                <th>{{ trans('admin_section.columns.title') }}</th>
                                <th>{{ trans('admin_section.columns.updated_at') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody class="sortable">
                            @foreach($sections as $section)
                            <tr data-section_id="{{ $section->id }}">
                                <td>{{ $section->id }}</td>
                                <td>{{ $section->title }}</td>
                                <td>{{ $section->updated_at }}</td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                        href="{{ route('admin.section.show',
                                        ['section' => $section->id]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-warning"
                                        href="{{ route('admin.section.edit',
                                        ['section' => $section->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a class="btn btn-xs btn-danger"
                                        href="#destroyModal" data-toggle="modal"
                                        data-url="{{ route('admin.section.destroy',
                                        ['section' => $section->id]) }}"
                                        data-text="{{ trans('common.destroy_section',
                                        ['id' => $section->id]) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#sections').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection
