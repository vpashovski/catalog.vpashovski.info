<div class="box-body">
    <div class="form-group">
        {!! Form::label('title', trans('admin_section.fields.title')) !!}
        {!! Form::text('title', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_section.fields.title')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', trans('admin_section.fields.slug')) !!}
        {!! Form::text('slug', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_section.fields.slug')]) !!}
    </div>
     <div class="form-group">
         {!! Form::label('description', trans('admin_section.fields.description')) !!}
         {!! Form::textarea('description', null,
         ['class' => 'form-control', 'rows' => '5',
         'placeholder' => trans('admin_section.fields.description')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('order', trans('admin_section.fields.order')) !!}
        {!! Form::number('order', null,
        ['class' => 'form-control', 'min' => '1', 'max' => '999', 'required' => 'required',
        'placeholder' => trans('admin_section.fields.order')]) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.section.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    <script>
        jQuery(function($) {
            $('.section-form').validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 255
                    },
                    slug: {
                        required: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.section.slug') }}'
                    },
                    order: {
                        required: true,
                        digits: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        $('.select2').select2();
    </script>
@endsection