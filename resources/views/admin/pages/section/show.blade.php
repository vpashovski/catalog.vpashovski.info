@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_section.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_section.fields.title') !!}</th>
                                <td>{{ $section->title }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_section.fields.slug') !!}</th>
                                <td>{{ $section->slug }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_section.fields.description') !!}</th>
                                <td>{{ $section->description }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.section.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.section.edit', ['section' => $section->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_section.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
