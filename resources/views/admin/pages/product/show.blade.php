@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_product.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            @foreach($product->getData('*', true, false) as $key => $value)
                                <tr>
                                    <th>{{ $key }}</th>
                                    <td>{{ $value }}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <th>{!! trans('admin_product.fields.category') !!}</th>
                                    <td>{{ $product->categories()->first()->name }}</td>
                                </tr>
                                <tr>
                                    <th>{!! trans('admin_product.fields.shop') !!}</th>
                                    <td>
                                        <a href="{{ route('admin.shop.show', ['shop' => $product->shop]) }}">
                                            {{ $product->shop->name }}
                                        </a>
                                    </td>
                                </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.product.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('common.image') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        @if($product->image)
                            {!! Html::image($product->image->url, 'Product image', ['width' => 275, 'height' => 275]) !!}
                        @else
                            {!! Html::image('uploads/images/no-image.png', 'Product image', ['width' => 275, 'height' => 275]) !!}
                        @endif
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
