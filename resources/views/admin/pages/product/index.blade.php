@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_product.all') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                    <div class="padd">
                        <div class="page-tables">
                            <table id="products" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{{ trans('admin_product.columns.id') }}</th>
                                    <th>{{ trans('admin_product.columns.image') }}</th>
                                    <th>{{ trans('admin_product.columns.name') }}</th>
                                    <th>{{ trans('admin_product.columns.shop') }}</th>
                                    <th>{{ trans('admin_product.columns.created_at') }}</th>
                                    <th>{{ trans('admin_product.columns.updated_at') }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                        <div class="widget-foot">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#products').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: [1, 5]
                }],
                "ajax":{
                    "url": "{{ route('admin.product.allproducts') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "processing": true,
                "serverSide": true,
                "columns": [
                    {"data": "id"},
                    {"data": "image"},
                    {"data": "name"},
                    {"data": "shop"},
                    {"data": "created_at"},
                    {"data": "updated_at"},
                    {"data": "show"}
                ],
            });
        });
    </script>
@endsection