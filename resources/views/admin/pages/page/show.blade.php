@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_page.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_page.fields.title') !!}</th>
                                <td>{{ $page->title }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_page.fields.url') !!}</th>
                                <td>{{ $page->url }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_page.fields.in_footer') !!}</th>
                                <td>{{ $page->in_footer }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_page.fields.content') !!}</th>
                                <td>{!! $page->content !!}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.page.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.page.edit', ['page' => $page->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_page.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
