<div class="box-body">
    <div class="form-group">
        {!! Form::label('title', trans('admin_page.fields.title')) !!}
        {!! Form::text('title', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_page.fields.title')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('url', trans('admin_page.fields.url')) !!}
        {!! Form::text('url', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_page.fields.url')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('content', trans('admin_page.fields.content')) !!}
        {!! Form::textarea('content', null,
        ['class' => 'form-control', 'rows' => '10',
        'required' => 'required',
        'placeholder' => trans('admin_page.fields.content')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('in_footer', trans('admin_page.fields.in_footer')) !!}
        {!! Form::select('in_footer', [1 => trans('common.yes'), 0 => trans('common.no')], null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.page.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/admin/ckeditor/ckeditor.js') !!}
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    <script>
        jQuery(function($) {
            $('.page-form').validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 255
                    },
                    url: {
                        required: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.page.url') }}'
                    },
                    content: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        $('.select2').select2();

        CKEDITOR.replace('content');
    </script>
@endsection