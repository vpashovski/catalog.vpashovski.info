@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_page.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.page.create') }}">
                    {{ trans('admin_page.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                        <table id="pages" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_page.columns.id') }}</th>
                                <th>{{ trans('admin_page.columns.title') }}</th>
                                <th>{{ trans('admin_page.columns.url') }}</th>
                                <th>{{ trans('admin_page.columns.in_footer') }}</th>
                                <th>{{ trans('admin_page.columns.updated_at') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->id }}</td>
                                <td>{{ $page->title }}</td>
                                <td>{{ $page->url }}</td>
                                <td>
                                    @if ($page->in_footer)
                                        <span class="label label-success"><i class="fa fa-check"></i> {{ trans('common.yes') }}</span>
                                    @else
                                        <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('common.no') }}</span>
                                    @endif
                                </td>
                                <td>{{ $page->updated_at }}</td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                        href="{{ route('admin.page.show',
                                        ['page' => $page->id]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-warning"
                                        href="{{ route('admin.page.edit',
                                        ['page' => $page->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a class="btn btn-xs btn-danger"
                                        href="#destroyModal" data-toggle="modal"
                                        data-url="{{ route('admin.page.destroy',
                                        ['page' => $page->id]) }}"
                                        data-text="{{ trans('common.destroy_page',
                                        ['id' => $page->id]) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>


    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#pages').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection