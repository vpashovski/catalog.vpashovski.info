@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_log.all') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                    <div class="padd">
                        <div class="page-tables">
                            <table id="logs" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{{ trans('admin_log.columns.id') }}</th>
                                    <th>{{ trans('admin_log.columns.shop') }}</th>
                                    <th>{{ trans('admin_log.columns.total_inserted_products') }}</th>
                                    <th>{{ trans('admin_log.columns.total_updated_products') }}</th>
                                    <th>{{ trans('admin_log.columns.total_deleted_products') }}</th>
                                    <th>{{ trans('admin_log.columns.total_error_products') }}</th>
                                    <th>{{ trans('admin_log.columns.created_at') }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td>{{ $log->id }}</td>
                                        <td>
                                            @if($log->shop)
                                                <a href="{{ route('admin.shop.show', ['shop' => $log->shop]) }}">
                                                    {{ $log->shop->name }}
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $log->total_inserted_products }}</td>
                                        <td>{{ $log->total_updated_products }}</td>
                                        <td>{{ $log->total_deleted_products }}</td>
                                        <td>{{ $log->total_error_products }}</td>
                                        <td>{{ $log->created_at }}</td>
                                        <td>
                                            <a class="btn btn-xs btn-danger"
                                               href="#destroyModal" data-toggle="modal"
                                               data-url="{{ route('admin.log.destroy', ['log' => $log->id]) }}"
                                               data-text="{{ trans('common.destroy_log', ['id' => $log->id]) }}"
                                            >
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="widget-foot">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#logs').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection
