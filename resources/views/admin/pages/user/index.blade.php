@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_user.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
              <a class="pull-right btn btn-xs btn-success"
                 href="{{ route('admin.user.create') }}">
                  {{ trans('admin_user.create') }}
              </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                        <table id="users" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_user.columns.id') }}</th>
                                <th>{{ trans('admin_user.columns.name') }}</th>
                                <th>{{ trans('admin_user.columns.email') }}</th>
                                <th>{{ trans('admin_user.columns.is_admin') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if ($user->is_admin)
                                            <span class="label label-success"><i class="fa fa-check"></i> {{ trans('common.yes') }}</span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('common.no') }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-success"
                                            href="{{ route('admin.user.show',
                                            ['user' => $user->id]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>

                                        <a class="btn btn-xs btn-warning"
                                            href="{{ route('admin.user.edit',
                                            ['user' => $user->id]) }}">
                                            <i class="fa fa-edit"></i>
                                        </a>

                                        <a class="btn btn-xs btn-danger"
                                            href="#destroyModal" data-toggle="modal"
                                            data-url="{{ route('admin.user.destroy',
                                            ['user' => $user->id]) }}"
                                            data-text="{{ trans('common.destroy_user',
                                            ['id' => $user->id, 'email' => $user->email]) }}">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                   <div class="widget-foot">
                   </div>
              </div>
          </div>
        </div>
      </div>
    </div>

    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#users').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection
