@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <!-- Widget title -->
          <div class="widget-head">
            <div class="pull-left">{!! trans('admin_user.show') !!}</div>
            <div class="widget-icons pull-right">
              <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a> 
            </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <!-- Widget content -->
            
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th>{!! trans('admin_user.fields.name') !!}</th>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <th>{!! trans('admin_user.fields.email') !!}</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>{!! trans('admin_user.fields.is_admin') !!}</th>
                    <td>
                        @if($user->is_admin)
                            <a class="btn btn-xs btn-success"
                               href="#">
                                <i class="fa fa-check"></i>
                            </a>
                        @else
                            <a class="btn btn-xs btn-danger"
                               href="#">
                                <i class="fa fa-close"></i>
                            </a>
                        @endif
                    </td>
                </tr>
              </table>
            </div>
            <div class="widget-foot">
              <a class="btn  btn-sm btn-default"
                 href="{{ route('admin.user.index') }}">
                  {{ trans('common.button.back') }}
              </a>
              <a href="{{ route('admin.user.edit', ['user' => $user->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_user.edit') !!}</b></a>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection