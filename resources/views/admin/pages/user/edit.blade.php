@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
            <div class="widget-head">
              <div class="pull-left">{{ trans('admin_user.edit') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <div class="padd">
                    <div class="form">
                      <!-- Edit profile form (not working)-->
                      <!-- form start -->
                      {!! Form::model($user, ['method' => 'PATCH', 'files' => true,
                          'url' => route('admin.user.update', ['user' => $user]),
                          'class' => 'user-form']) !!}
                          @include('admin.pages.user._form')
                      {!! Form::close() !!}
                      <!-- form end -->
                    </div>
                </div>
                <div class="widget-foot">
                <!-- Footer goes here -->
                </div>
            </div>
        </div>
      </div>
    </div>
@endsection