<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', trans('admin_user.fields.name')) !!}
        {!! Form::text('name', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_user.fields.name')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', trans('admin_user.fields.email')) !!}
        <div class="input-group">
            <span class="input-group-addon">@</span>
            {!! Form::email('email', null,
            ['class' => 'form-control', 'required' => 'required',
            'placeholder' => trans('admin_user.fields.email')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('password', trans('admin_user.fields.password')) !!}
        {!! Form::password('password',
        ['class' => 'form-control',
        'placeholder' => trans('admin_user.fields.password')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password_confirmation', trans('admin_user.fields.password_confirmation')) !!}
        {!! Form::password('password_confirmation',
        ['class' => 'form-control',
        'placeholder' => trans('admin_user.fields.password_confirmation')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('is_admin', trans('admin_user.fields.is_admin')) !!}
        {!! Form::select('is_admin', [0 => trans('common.no'), 1 => trans('common.yes')], null,
        ['class' => 'form-control',  'required' => 'required']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.user.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>


@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/jquery.validate.messages_bg.min.js') !!}

    <script>
        jQuery(function($) {
            $('.user-form').validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 255
                    },
                    email: {
                        required: true,
                        email: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.user.email') }}'
                    },
                    password: {
                        minlength: 6

                    },
                    password_confirmation: {
                        equalTo: 'input[name="password"]'
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
@endsection
