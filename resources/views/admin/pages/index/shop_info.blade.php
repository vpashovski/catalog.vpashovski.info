<!-- Chats, File upload and Recent Comments -->
<div class="row">
    @if($latest_shops)
    <div class="col-md-6">
        <!-- Widget -->
        <div class="widget">
            <!-- Widget title -->
            <div class="widget-head">
                <div class="pull-left">{{ trans('admin_index.latest_shops') }}</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <!-- Widget content -->
                <div class="padd">
                    <ul class="recent">
                        @foreach($latest_shops as $latest_shop)
                        <li>
                            <div class="recent-content">
                                <div class="recent-meta">
                                    <span><i class="fa fa-clock-o"></i> {{ $latest_shop->created_at->format('Y-m-d') }}</span>
                                </div>
                                <div>
                                    <a href="{{ route('admin.shop.show',
                                    ['shop' => $latest_shop->id]) }}">
                                        {{ $latest_shop->name }}
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </li>
                            @endforeach
                    </ul>
                </div>
                <!-- Widget footer -->
                <div class="widget-foot">
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($most_visited_shops)
    <div class="col-md-6">
        <!-- Widget -->
        <div class="widget">
            <!-- Widget title -->
            <div class="widget-head">
                <div class="pull-left">{{ trans('admin_index.most_visited_shops') }}</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <!-- Widget content -->
                <div class="padd">
                    <ul class="recent">
                        @foreach($most_visited_shops as $most_visited_shop)
                            <li>
                                <div class="recent-content">
                                    <div class="recent-meta">
                                        <span><i class="fa fa-clock-o"></i> {{ $most_visited_shop->created_at->format('Y-m-d') }}</span>
                                    </div>
                                    <div>
                                        <a href="{{ route('admin.shop.show',
                                    ['shop' => $most_visited_shop->id]) }}">
                                            {{ $most_visited_shop->name }}
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- Widget footer -->
                <div class="widget-foot">
                </div>
            </div>
        </div>
    </div>
    @endif
</div>