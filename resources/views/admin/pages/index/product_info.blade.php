<!-- Chats, File upload and Recent Comments -->
<div class="row">
    @if($latest_products)
        <div class="col-md-6">
            <!-- Widget -->
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_index.latest_products') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                        <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="padd">
                        <ul class="recent">
                            @foreach($latest_products as $latest_product)
                            <li>
                                <div class="recent-content">
                                    <div class="recent-meta">
                                        <span><i class="fa fa-clock-o"></i> {{ $latest_product->created_at->format('Y-m-d') }}</span>
                                    </div>
                                    <div>
                                        <a href="{{ route('admin.product.show',
                                        ['product' => $latest_product->id]) }}">
                                            {{ $latest_product->getData(\App\Product::NAME) }}
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- Widget footer -->
                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($most_visited_products)
    <div class="col-md-6">
        <!-- Widget -->
        <div class="widget">
            <!-- Widget title -->
            <div class="widget-head">
                <div class="pull-left">{{ trans('admin_index.most_visited_products') }}</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <!-- Widget content -->
                <div class="padd">
                    <ul class="recent">
                        @foreach($most_visited_products as $most_visited_product)
                            <li>
                                <div class="recent-content">
                                    <div class="recent-meta">
                                        <span><i class="fa fa-clock-o"></i> {{ $most_visited_product->created_at->format('Y-m-d') }}</span>
                                    </div>
                                    <div>
                                        <a href="{{ route('admin.product.show',
                                    ['product' => $most_visited_product->id]) }}">
                                            {{ $most_visited_product->getData(\App\Product::NAME) }}
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- Widget footer -->
                <div class="widget-foot">
                </div>
            </div>
        </div>
    </div>
    @endif
</div>