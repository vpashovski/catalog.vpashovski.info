<!-- Chats, File upload and Recent Comments -->
<div class="row">
    @if($latest_articles)
    <div class="col-md-6">
        <!-- Widget -->
        <div class="widget">
            <!-- Widget title -->
            <div class="widget-head">
                <div class="pull-left">{{ trans('admin_index.latest_articles') }}</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <!-- Widget content -->
                <div class="padd">
                    <ul class="recent">
                        @foreach($latest_articles as $latest_article)
                        <li>
                            <div class="recent-content">
                                <div class="recent-meta">
                                    <span><i class="fa fa-clock-o"></i> {{ $latest_article->created_at->format('Y-m-d') }}</span>
                                    <span class="pull-right"><i class="fa fa-user"></i> {{ $latest_article->user->name }}</span>
                                </div>
                                <div>
                                    <a href="{{ route('admin.article.show',
                                    ['article' => $latest_article->id]) }}">
                                        {{ $latest_article->title }}
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- Widget footer -->
                <div class="widget-foot">
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($most_visited_articles)
    <div class="col-md-6">
        <!-- Widget -->
        <div class="widget">
            <!-- Widget title -->
            <div class="widget-head">
                <div class="pull-left">{{ trans('admin_index.most_visited_articles') }}</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <!-- Widget content -->
                <div class="padd">
                    <ul class="recent">
                        @foreach($most_visited_articles as $most_visited_article)
                            <li>
                                <div class="recent-content">
                                    <div class="recent-meta">
                                        <span><i class="fa fa-clock-o"></i> {{ $most_visited_article->created_at->format('Y-m-d') }}</span>
                                        <span class="pull-right"><i class="fa fa-user"></i> {{ $most_visited_article->user->name }}</span>
                                    </div>
                                    <div>
                                        <a href="{{ route('admin.article.show',
                                    ['article' => $most_visited_article->id]) }}">
                                            {{ $most_visited_article->title }}
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- Widget footer -->
                <div class="widget-foot">
                </div>
            </div>
        </div>
    </div>
    @endif
</div>