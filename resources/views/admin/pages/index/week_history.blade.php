<div class="row">
    <div class="col-md-12">


        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">{{ trans('admin_index.weekly_details') }}</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>


            <div class="widget-content">
                <div class="padd">
                    <div id="curve-chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer_script')
    <script>
        $(function () {
            var products = [], shops = [], articles = [];
            @foreach ($chart_data as $type => $data)
                @foreach ($data as $key => $value)
                    @if ($type == 'products')
                        products.push(['{{ $key }}', '{{ $value }}']);
                    @elseif ($type == 'shops')
                        shops.push(['{{ $key }}', '{{ $value }}']);
                    @elseif ($type == 'articles')
                        articles.push(['{{ $key }}', '{{ $value }}']);
                    @endif
                @endforeach
            @endforeach

            var plot = $.plot($("#curve-chart"),
                [
                    { data: products, label: '{{ trans('admin_index.products') }}' },
                    { data: shops, label: '{{ trans('admin_index.shops') }}' },
                    { data: articles, label: '{{ trans('admin_index.articles') }}' }
                ], {
                    series: {
                        lines: { show: true},
                        points: { show: true }
                    },
                    grid: { hoverable: true, borderWidth:0 },
                    xaxis:  {
                        mode: "time",
                    },
                    yaxis:  {
                        min:0,
                        max: '{{ $max_value }}',
                        tickSize: 5
                    },
                }
            );
        });
    </script>
@endsection