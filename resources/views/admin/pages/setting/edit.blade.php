@extends('layouts.admin')

@section('content')
    <div class="row">
        {!! Form::open(['method' => 'POST', 'files' => true,
            'url' => route('admin.setting.update', ['setting' => null]),
            'class' => 'setting-form']) !!}
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_setting.edit') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        <div class="form">
                            <div class="box-body">
                                <ul id="settingsTab" class="nav nav-tabs">
                                    <li class="active"><a href="#base" data-toggle="tab">{{ trans('admin_setting.tabs.base') }}</a></li>
                                    <li><a href="#contact" data-toggle="tab">{{ trans('admin_setting.tabs.contact') }}</a></li>
                                </ul>
                                <div id="settingsTabContent" class="tab-content">
                                    <div class="tab-pane fade in active" id="base">
                                        <div class="form-group">
                                            {!! Form::label('site_name', trans('admin_setting.fields.site_name')) !!}
                                            {!! Form::text('site_name', Setting::get('site_name'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.site_name')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('site_description', trans('admin_setting.fields.site_description')) !!}
                                            {!! Form::text('site_description', Setting::get('site_description'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.site_description')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('frontend_color', trans('admin_setting.fields.frontend_color')) !!}
                                            {!! Form::select('frontend_color', [
                                                'blue'      => trans('admin_setting.color_options.blue'),
                                                'brown'     => trans('admin_setting.color_options.brown'),
                                                'green'     => trans('admin_setting.color_options.green'),
                                                'orange'    => trans('admin_setting.color_options.orange'),
                                                'red'       => trans('admin_setting.color_options.red'),
                                                'yellow'    => trans('admin_setting.color_options.yellow'),
                                            ], Setting::get('frontend_color'),
                                            ['class' => 'form-control select2',  'required' => 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('about_us', trans('admin_setting.fields.about_us')) !!}
                                            {!! Form::textarea('about_us', Setting::get('about_us'),
                                            ['class' => 'form-control', 'rows' => '5',
                                            'placeholder' => trans('admin_setting.fields.about_us')]) !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="contact">
                                        <div class="form-group">
                                            {!! Form::label('contact_name', trans('admin_setting.fields.contact_name')) !!}
                                            {!! Form::text('contact_name', Setting::get('contact_name'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.contact_name')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('contact_address', trans('admin_setting.fields.contact_address')) !!}
                                            {!! Form::text('contact_address', Setting::get('contact_address'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.contact_address')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('full_contact_address', trans('admin_setting.fields.full_contact_address')) !!}
                                            {!! Form::textarea('full_contact_address', Setting::get('full_contact_address'),
                                            ['class' => 'form-control', 'rows' => '5',
                                            'placeholder' => trans('admin_setting.fields.full_contact_address')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('contact_phone', trans('admin_setting.fields.contact_phone')) !!}
                                            {!! Form::text('contact_phone', Setting::get('contact_phone'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.contact_phone')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('contact_email', trans('admin_setting.fields.contact_email')) !!}
                                            {!! Form::text('contact_email', Setting::get('contact_email'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.contact_email')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('google_map', trans('admin_setting.fields.google_map')) !!}
                                            {!! Form::textarea('google_map', Setting::get('google_map'),
                                            ['class' => 'form-control', 'rows' => '3',
                                            'placeholder' => trans('admin_setting.fields.google_map')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('facebook', trans('admin_setting.fields.facebook')) !!}
                                            {!! Form::text('facebook', Setting::get('facebook'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.facebook')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('twitter', trans('admin_setting.fields.twitter')) !!}
                                            {!! Form::text('twitter', Setting::get('twitter'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.twitter')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('linkedin', trans('admin_setting.fields.linkedin')) !!}
                                            {!! Form::text('linkedin', Setting::get('linkedin'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.linkedin')]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('google_plus', trans('admin_setting.fields.google_plus')) !!}
                                            {!! Form::text('google_plus', Setting::get('google_plus'),
                                            ['class' => 'form-control',
                                            'placeholder' => trans('admin_setting.fields.google_plus')]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <div class="box-footer">
                                    <a class="btn btn-default"
                                       href="{{ route('admin.index') }}">
                                        {{ trans('common.button.back') }}
                                    </a>
                                    {!! Form::submit(trans("common.button.submit"),
                                        ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/admin/select2/select2.js') !!}

    <script>
    $('.select2').select2();
    </script>
@endsection