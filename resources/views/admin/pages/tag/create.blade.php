@extends('layouts.admin')

@section('content')
    <div class="row">
      {!! Form::open(['method' => 'POST', 'files' => true,
          'url' => route('admin.tag.store'),
          'class' => 'tag-form']) !!}
        <div class="col-md-12">
          <div class="widget">
              <div class="widget-head">
                <div class="pull-left">{{ trans('admin_tag.create') }}</div>
                <div class="widget-icons pull-right">
                  <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="widget-content">
                  <div class="padd">
                      <div class="form">
                        <!-- Edit profile form (not working)-->
                        <!-- form start -->
                        @include('admin.pages.tag._form')
                        <!-- form end -->
                      </div>
                  </div>
                  <div class="widget-foot">
                  <!-- Footer goes here -->
                  </div>
              </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
@endsection