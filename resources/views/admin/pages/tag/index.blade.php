@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_tag.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.tag.create') }}">
                    {{ trans('admin_tag.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                        <table id="tags" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_tag.columns.id') }}</th>
                                <th>{{ trans('admin_tag.columns.title') }}</th>
                                <th>{{ trans('admin_tag.columns.updated_at') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($tags as $tag)
                            <tr>
                                <td>{{ $tag->id }}</td>
                                <td>{{ $tag->title }}</td>
                                <td>{{ $tag->updated_at }}</td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                        href="{{ route('admin.tag.show',
                                        ['tag' => $tag->id]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-warning"
                                        href="{{ route('admin.tag.edit',
                                        ['tag' => $tag->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a class="btn btn-xs btn-danger"
                                        href="#destroyModal" data-toggle="modal"
                                        data-url="{{ route('admin.tag.destroy',
                                        ['tag' => $tag->id]) }}"
                                        data-text="{{ trans('common.destroy_tag',
                                        ['id' => $tag->id]) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#tags').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection
