@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('admin.pages.stat._filter')

            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_stat.products') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('admin_stat.columns.id') }}</th>
                                <th>{{ trans('admin_stat.columns.unique_views') }}</th>
                                <th>{{ trans('admin_stat.columns.total_views') }}</th>
                                <th>{{ trans('admin_stat.columns.last_visited') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ $item->item_id }}</td>
                                    <td>{{ $item->unique_views }}</td>
                                    <td>{{ $item->total_views }}</td>
                                    <td>{{ $item->last_visited }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="widget-foot">
                        {!! $items->links() !!}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection