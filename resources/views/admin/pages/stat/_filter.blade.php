<div class="widget">
    <div class="widget-head">
        <div class="pull-left">{{ trans('admin_stat.filter') }}</div>
        <div class="widget-icons pull-right">
            <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
            <a href="#" class="wclose"><i class="fa fa-times"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="widget-content">
        <div class="padd">
            <div class="form">
                {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    <div class="col-sm-1">
                        {!! Form::text('item_id', Request::input('item_id'),
                        ['class' => 'form-control input-sm',
                        'placeholder' => trans('admin_stat.columns.id')]) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('unique_views', Request::input('unique_views'),
                        ['class' => 'form-control input-sm',
                        'placeholder' => trans('admin_stat.columns.unique_views')]) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('total_views', Request::input('total_views'),
                        ['class' => 'form-control input-sm',
                        'placeholder' => trans('admin_stat.columns.total_views')]) !!}
                    </div>
                    <div class="col-sm-2">
                        <div id="last_visited" class="input-append input-group dtpicker">
                            <input name="last_visited" value="{{ Request::input('last_visited') }}" placeholder="{{ trans('admin_stat.columns.last_visited') }}" data-format="yyyy-MM-dd" type="text" class="form-control">
                            <span class="input-group-addon add-on">
                                <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        {!! Form::select('order_by', [
                            ''       => trans('admin_stat.order_by'),
                            'item_id'       => trans('admin_stat.columns.id'),
                            'unique_views'  => trans('admin_stat.columns.unique_views'),
                            'total_views'   => trans('admin_stat.columns.total_views'),
                            'last_visited'  => trans('admin_stat.columns.last_visited'),
                        ], Request::input('order_by'),
                        ['class' => 'form-control', ]) !!}
                    </div>
                    <div class="col-sm-1">
                        {!! Form::select('order', [
                            'desc'  => trans('admin_stat.desc'),
                            'asc'   => trans('admin_stat.asc'),
                        ], Request::input('order'),
                        ['class' => 'form-control', ]) !!}
                    </div>
                    <div class="col-sm-2 pull-right">
                        {!! Form::submit(trans('common.search'),
                        ['class' => 'btn btn-sm btn-primary col-sm-5 col-sm-offset-1']) !!}
                        <a class="btn btn-sm btn-warning col-sm-5 col-sm-offset-1"
                           href="{{ route(\Request::route()->getName()) }}">
                            {{ trans('common.reset_filter') }}
                        </a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="widget-foot">
            <!-- Footer goes here -->
        </div>
    </div>
</div>

@section('footer_script')
    <script>
        $(function() {
            $('#last_visited').datetimepicker({
                pickTime: false
            });
        });

    </script>
@endsection
