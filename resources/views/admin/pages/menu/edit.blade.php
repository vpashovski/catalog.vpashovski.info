@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
            <div class="widget-head">
              <div class="pull-left">{{ trans('admin_menu.edit') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <div class="padd">
                    <div class="form">
                      <!-- Edit profile form (not working)-->
                      <!-- form start -->
                      {!! Form::model($menu, ['method' => 'PATCH', 'files' => true,
                          'url' => route('admin.menu.update', ['menu' => $menu]),
                          'class' => 'menu-form']) !!}
                          @include('admin.pages.menu._form')
                      {!! Form::close() !!}
                      <!-- form end -->
                    </div>
                </div>
                <div class="widget-foot">
                <!-- Footer goes here -->
                </div>
            </div>
        </div>
      </div>
    </div>
@endsection