@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_menu.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.menu.create') }}">
                    {{ trans('admin_menu.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                            <th>{{ trans('admin_menu.columns.id') }}</th>
                            <th>{{ trans('admin_menu.columns.title') }}</th>
                            <th>{{ trans('admin_menu.columns.url') }}</th>
                            <th>{{ trans('admin_menu.columns.updated_at') }}</th>
                            <th></th>
                        </tr>
                      </thead>
                      <tbody class="sortable">
                        @foreach($menus as $menu)
                        <tr data-menu_id="{{ $menu->id }}">
                            <td>{{ $menu->id }}</td>
                            <td>{{ $menu->title }}</td>
                            <td>{{ $menu->url }}</td>
                            <td>{{ $menu->updated_at }}</td>
                            <td>
                                <a class="btn btn-xs btn-success"
                                    href="{{ route('admin.menu.show',
                                    ['menu' => $menu->id]) }}">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a class="btn btn-xs btn-warning"
                                    href="{{ route('admin.menu.edit',
                                    ['menu' => $menu->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <a class="btn btn-xs btn-danger"
                                    href="#destroyModal" data-toggle="modal"
                                    data-url="{{ route('admin.menu.destroy',
                                    ['menu' => $menu->id]) }}"
                                    data-text="{{ trans('common.destroy_menu',
                                    ['id' => $menu->id]) }}">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>

               <div class="widget-foot">
                   <a href="javascript:void(0);" class="btn btn-primary pull-right save-order">
                       {{ trans('common.save_order') }} <i class="fa"></i>
                   </a>
                  <div class="clearfix"></div>
               </div>
          </div>
        </div>
      </div>
    </div>

    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script>
        jQuery(function($) {
            $('.save-order').on('click', function() {
                $.ajax({
                    method: 'POST',
                    url: '{{ route('admin.menu.order') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        order: $('.sortable').sortable('toArray', {attribute: 'data-menu_id'})
                    },
                    success: function() {
                        $('.save-order').removeClass('btn-primary').removeClass('btn-danger').addClass('btn-success');
                        $('.save-order i').removeClass('fa-close').addClass('fa-check');
                    },
                    error: function() {
                        $('.save-order').removeClass('btn-primary').removeClass('btn-success').addClass('btn-danger');
                        $('.save-order i').removeClass('fa-check').addClass('fa-close');
                    }
                });
            });

            $('.sortable').sortable({
                change: function( event, ui ) {
                    $('.save-order').removeClass('btn-success').removeClass('btn-danger').addClass('btn-primary');
                    $('.save-order i').removeClass('fa-check').removeClass('fa-close');
                }
            });
        });
    </script>
@endsection
