<div class="box-body">
    <div class="form-group">
        {!! Form::label('title', trans('admin_menu.fields.title')) !!}
        {!! Form::text('title', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_menu.fields.title')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('url', trans('admin_menu.fields.url')) !!}
        {!! Form::text('url', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_menu.fields.url')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('order', trans('admin_menu.fields.order')) !!}
        {!! Form::number('order', null,
        ['class' => 'form-control', 'min' => '1', 'max' => '999', 'required' => 'required',
        'placeholder' => trans('admin_menu.fields.order')]) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.menu.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>


@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}

    <script>
        jQuery(function($) {
            $('.menu-form').validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 255
                    },
                    url: {
                        required: true,
                        maxlength: 255
                    },
                    order: {
                        required: true,
                        digits: true
                    }
                }
            });
        });
    </script>
@endsection
