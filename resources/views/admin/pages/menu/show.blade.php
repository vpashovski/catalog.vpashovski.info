@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <!-- Widget title -->
          <div class="widget-head">
            <div class="pull-left">{!! trans('admin_menu.show') !!}</div>
            <div class="widget-icons pull-right">
              <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a> 
            </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <!-- Widget content -->
            
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th>{!! trans('admin_menu.fields.title') !!}</th>
                    <td>{{ $menu->title }}</td>
                </tr>
                <tr>
                    <th>{!! trans('admin_menu.fields.url') !!}</th>
                    <td>{{ $menu->url }}</td>
                </tr>
                  <tr>
                      <th>{!! trans('admin_menu.fields.order') !!}</th>
                      <td>{{ $menu->order }}</td>
                  </tr>
              </table>
            </div>
            <div class="widget-foot">
              <a class="btn  btn-sm btn-default"
                 href="{{ route('admin.menu.index') }}">
                  {{ trans('common.button.back') }}
              </a>
              <a href="{{ route('admin.menu.edit', ['menu' => $menu->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_menu.edit') !!}</b></a>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection