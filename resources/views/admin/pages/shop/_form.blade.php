<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', trans('admin_shop.fields.name')) !!}
        {!! Form::text('name', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_shop.fields.name')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', trans('admin_shop.fields.slug')) !!}
        {!! Form::text('slug', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_shop.fields.slug')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('feed_url', trans('admin_shop.fields.feed_url')) !!}
        {!! Form::text('feed_url', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_shop.fields.feed_url')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('shop_url', trans('admin_shop.fields.shop_url')) !!}
        {!! Form::text('shop_url', null,
        ['class' => 'form-control',
        'placeholder' => trans('admin_shop.fields.shop_url')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('shop_aff_url', trans('admin_shop.fields.shop_aff_url')) !!}
        {!! Form::text('shop_aff_url', null,
        ['class' => 'form-control',
        'placeholder' => trans('admin_shop.fields.shop_aff_url')]) !!}
    </div>
     <div class="form-group">
        {!! Form::label('attribute_set_id', trans('admin_shop.fields.attribute_set')) !!}
        {!! Form::select('attribute_set_id', $attribute_sets, null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('category_id', trans('admin_shop.fields.category')) !!}
        {!! Form::select('category_id', $categories, null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('cron_schedule', trans('admin_shop.fields.cron_schedule')) !!}
        <div id="cron_selector"></div>
        {!! Form::hidden('cron_schedule', null) !!}
    </div>
    <div class="form-group">
        {!! Form::label('status', trans('admin_shop.fields.status')) !!}
        {!! Form::select('status', [0 => trans('common.disabled'), 1 => trans('common.enabled')], null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.shop.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
    {!! Html::style('css/admin/cron/jquery-cron.css') !!}
    {!! Html::style('css/admin/gentleSelect/jquery-gentleSelect.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/admin/images.js') !!}
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    {!! Html::script('js/admin/cron/jquery-cron-min.js') !!}
    {!! Html::script('js/admin/gentleSelect/jquery-gentleSelect-min.js') !!}
    {{-- {!! Html::script('js/jquery.validate.messages_bg.min.js') !!} --}}
    <script>
        jQuery(function($) {
            $('.shop-form').validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 255
                    },
                    slug: {
                        required: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.shop.slug') }}'
                    },
                    feed_url: {
                        required: true,
                        url: true
                    },
                    shop_url: {
                        url: true
                    },
                    shop_aff_url: {
                        url: true
                    },
                    // attribute_set_id: {
                    //     required: true
                    // },
                    status: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        $('.select2').select2();

        @if (!isset($shop))
            var initial = '0 3 * * 5';
        @else
            var initial = '{{ $shop->cron_schedule }}';
        @endif
        $('#cron_selector').cron({
            initial: initial,
            onChange: function() {
                $('#cron_schedule').val($(this).cron('value'));
            },
            useGentleSelect: true // default: false
        });
    </script>
@endsection