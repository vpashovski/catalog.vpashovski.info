@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_shop.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_shop.fields.name') !!}</th>
                                <td>{{ $shop->name }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.slug') !!}</th>
                                <td>{{ $shop->slug }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.feed_url') !!}</th>
                                <td><a href="{{ $shop->feed_url }}" title="{{ $shop->slug }}">{{ $shop->feed_url }}</a></td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.shop_url') !!}</th>
                                <td><a href="{{ $shop->shop_url }}" title="{{ $shop->slug }}">{{ $shop->shop_url }}</a></td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.shop_aff_url') !!}</th>
                                <td><a href="{{ $shop->shop_aff_url }}" title="{{ $shop->slug }}">{{ $shop->shop_aff_url }}</a></td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.attribute_set') !!}</th>
                                <td>{{ $shop->attribute_set->name }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.category') !!}</th>
                                <td>{{ $shop->category->name }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.products_count') !!}</th>
                                <td>{{ $shop->products->count() }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.views') !!}</th>
                                <td>{{ $shop->views }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.cron_schedule') !!}</th>
                                <td>{{ $shop->human_readable_cron_schedule }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.rating') !!}</th>
                                <td><div class="rateit" data-rateit-value="{{ $shop->rating }}" data-rateit-readonly="true" data-rateit-resetable="false"></div></td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_shop.fields.status') !!}</th>
                                <td>
                                    @if($shop->status)
                                        <a class="btn btn-xs btn-success"
                                           href="#">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-xs btn-danger"
                                           href="#">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.shop.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.shop.edit', ['shop' => $shop->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_shop.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('common.image') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        @if($shop->image)
                            {!! Html::image($shop->image->url, 'selected image') !!}
                        @else
                            {!! Html::image('assets/img/no-image.png', 'selected image') !!}
                        @endif
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-8">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_shop.fields.cron_schedule') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        <div class="alert alert-warning alert-dismissible" id="sync_error_message" style="display: none;">
                            <div><i class="icon fa fa-exclamation faa-flash animated"></i> <span></span></div>
                        </div>
                        <div class="form">
                            <div class="form-group">
                                <button type="button" id="sync" class="btn btn-success col-sm-12">
                                    <i class="fa fa-refresh"></i> {{ trans('admin_shop.fields.cron_schedule') }}
                                </button>
                                <div class="clearfix"></div>
                                <div class="progress progress-striped active" style="display: none;" id="sync_progress_bar_container">
                                    <div id="sync_progress_bar" class="progress-bar" role="progressbar"
                                         aria-valuenow="100"
                                         aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width:0%;">
                                        <span id="sync_progress"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td> {{ trans('admin_log.columns.total_inserted_products') }}</td>
                                <td id="total_inserted_products"></td>
                            <tr>
                            <tr>
                                <td> {{ trans('admin_log.columns.total_updated_products') }}</td>
                                <td id="total_updated_products"></td>
                            <tr>
                            <tr>
                                <td> {{ trans('admin_log.columns.total_deleted_products') }}</td>
                                <td id="total_deleted_products"></td>
                            <tr>
                            <tr>
                                <td> {{ trans('admin_log.columns.total_error_products') }}</td>
                                <td id="total_error_products"></td>
                            <tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('head_styles')
    {!! Html::style('css/admin/cron/jquery-cron.css') !!}
    {!! Html::style('css/admin/gentleSelect/jquery-gentleSelect.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/admin/cron/jquery-cron-min.js') !!}
    {!! Html::script('js/admin/gentleSelect/jquery-gentleSelect-min.js') !!}
    <script>
        jQuery(function($) {
            $('#sync').on('click', function () {
                @if($shop->status)
                    var es = new EventSource('{{ route('admin.sync.index', ['store_id' => $shop->id]) }}');
                    $('#sync_progress_bar').width(0);
                    $('#sync_progress_bar_container').show();

                    //a message is received
                    es.addEventListener('message', function(e) {
                        var result = JSON.parse( e.data );

                        if(result.log != null) {
                            $('#total_inserted_products').text(result.log.total_inserted_products);
                            $('#total_updated_products').text(result.log.total_updated_products);
                            $('#total_deleted_products').text(result.log.total_deleted_products);
                            $('#total_error_products').text(result.log.total_error_products);

                            $('#sync').attr('disabled', false).find('.fa-refresh').removeClass('fa-spin');
                            es.close();
                        } else {
                            $('#sync').attr('disabled', true).find('.fa-refresh').addClass('fa-spin');
                            var progress = parseInt((result.current / result.count) * 100);

                            $('#sync_progress_bar').width(progress + '%');
                            $('#sync_progress').text(progress + '%');
                        }
                    });

                    es.addEventListener('error', function(e) {
                        es.close();
                    });
                @else
                    $('#sync_error_message').show();
                    $('#sync_error_message').find('span').text('{{ trans('admin_log.error_status') }}');
                @endif
            });
        });
    </script>
@endsection