@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_shop.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.shop.create') }}">
                    {{ trans('admin_shop.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                        <table id="shops" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_shop.columns.id') }}</th>
                                <th>{{ trans('admin_shop.columns.image') }}</th>
                                <th>{{ trans('admin_shop.columns.name') }}</th>
                                <th>{{ trans('admin_shop.columns.cron_schedule') }}</th>
                                <th>{{ trans('admin_shop.columns.updated_at') }}</th>
                                <th>{{ trans('admin_shop.columns.status') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($shops as $shop)
                            <tr>
                                <td>{{ $shop->id }}</td>
                                <td>
                                  @if($shop->image)
                                      {!! Html::image($shop->image->url,
                                          'Shop Image', ['width' => 30, 'height' => 30]) !!}
                                  @else
                                      {!! Html::image('assets/img/no-image.png',
                                       'Shop Image', ['width' => 30, 'height' => 30]) !!}
                                  @endif
                                </td>
                                <td>{{ $shop->name }}</td>
                                <td>{{ $shop->human_readable_cron_schedule }}</td>
                                <td>{{ $shop->updated_at }}</td>
                                <td>
                                    @if ($shop->status)
                                        <span class="label label-success"><i class="fa fa-check"></i> {{ trans('common.enabled') }}</span>
                                    @else
                                        <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('common.disabled') }}</span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                        href="{{ route('admin.shop.show',
                                        ['shop' => $shop->id]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-warning"
                                        href="{{ route('admin.shop.edit',
                                        ['shop' => $shop->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a class="btn btn-xs btn-danger"
                                        href="#destroyModal" data-toggle="modal"
                                        data-url="{{ route('admin.shop.destroy',
                                        ['shop' => $shop->id]) }}"
                                        data-text="{{ trans('common.destroy_shop',
                                        ['id' => $shop->id, 'email' => $shop->email]) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>


    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#shops').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: [1, 6]
                }]
            });
        });
    </script>
@endsection
