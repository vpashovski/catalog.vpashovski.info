<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', trans('admin_category.fields.name')) !!}
        {!! Form::text('name', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_category.fields.name')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', trans('admin_category.fields.slug')) !!}
        {!! Form::text('slug', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_category.fields.slug')]) !!}
    </div>
     <div class="form-group">
        {!! Form::label('parent_category_id', trans('admin_category.fields.parent_category')) !!}
        {!! Form::select('parent_category_id', $parent_categories, null,
        ['class' => 'form-control select2', 'placeholder' => 'No Parent']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.category.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    <script>
        jQuery(function($) {
            $('.category-form').validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 255
                    },
                    slug: {
                        required: true,
                        maxlength: 255,
                        remote: '{{ route('admin.validate.category.slug') }}'
                    },
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        $('.select2').select2();
    </script>
@endsection