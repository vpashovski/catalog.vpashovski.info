@extends('layouts.admin')

@section('content')
    <div class="row">
        {!! Form::model($category, ['method' => 'PATCH', 'files' => true,
            'url' => route('admin.category.update', ['category' => $category]),
            'class' => 'category-form']) !!}
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-head">
                    <div class="pull-left">{{ trans('admin_category.create') }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        <div class="form">
                            <!-- Edit profile form (not working)-->
                            <!-- form start -->
                        @include('admin.pages.category._form')
                        <!-- form end -->
                        </div>
                    </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection