@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_comment.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.comment.create') }}">
                    {{ trans('admin_comment.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                        <div class="page-tables">
                        <table id="comments" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_comment.columns.id') }}</th>
                                <th>{{ trans('admin_comment.columns.commentator_name') }}</th>
                                <th>{{ trans('admin_comment.columns.article') }}</th>
                                <th>{{ trans('admin_comment.columns.approved') }}</th>
                                <th>{{ trans('admin_comment.columns.updated_at') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($comments as $comment)
                            <tr>
                                <td>
                                    {{ $comment->id }} @if(!$comment->checked) <span class="label label-info">{{ trans('common.new') }}</span> @endif</td>
                                <td>{{ $comment->commentator_name }} @if($comment->commentator_email) ({{ $comment->commentator_email }}) @endif</td>
                                <td>
                                    @if($comment->article)
                                        <a href="{{ route('admin.article.show',
                                            ['article' => $comment->article->id]) }}">
                                            {{ $comment->article->title }}
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if ($comment->approved)
                                        <span class="label label-success"><i class="fa fa-check"></i> {{ trans('common.yes') }}</span>
                                    @else
                                        <span class="label label-danger"><i class="fa fa-close"></i> {{ trans('common.no') }}</span>
                                    @endif
                                </td>
                                <td>{{ $comment->updated_at }}</td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                        href="{{ route('admin.comment.show',
                                        ['comment' => $comment->id]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-warning"
                                        href="{{ route('admin.comment.edit',
                                        ['comment' => $comment->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a class="btn btn-xs btn-danger"
                                        href="#destroyModal" data-toggle="modal"
                                        data-url="{{ route('admin.comment.destroy',
                                        ['comment' => $comment->id]) }}"
                                        data-text="{{ trans('common.destroy_comment',
                                        ['id' => $comment->id]) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#comments').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection
