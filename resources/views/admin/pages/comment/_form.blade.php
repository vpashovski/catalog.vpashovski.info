<div class="box-body">
    <div class="form-group">
        {!! Form::label('commentator_name', trans('admin_comment.fields.commentator_name')) !!}
        {!! Form::text('commentator_name', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_comment.fields.commentator_name')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('commentator_email', trans('admin_comment.fields.commentator_email')) !!}
        {!! Form::text('commentator_email', null,
        ['class' => 'form-control',
        'placeholder' => trans('admin_comment.fields.commentator_email')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('comment', trans('admin_comment.fields.comment')) !!}
        {!! Form::textarea('comment', null,
        ['class' => 'form-control', 'rows' => '5', 'required' => 'required',
        'placeholder' => trans('admin_comment.fields.comment')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('article_id', trans('admin_comment.fields.article')) !!}
        {!! Form::select('article_id', $articles, null,
        ['class' => 'form-control select2',  'required' => 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('approved', trans('admin_comment.fields.approved')) !!}
        {!! Form::select('approved', [0 => trans('common.no'), 1 => trans('common.yes')], null,
        ['class' => 'form-control',  'required' => 'required']) !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.comment.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/admin/images.js') !!}
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    <script>
        jQuery(function($) {
            $('.comment-form').validate({
                rules: {
                    commentator_name: {
                        required: true,
                        maxlength: 255
                    },
                    comment: {
                        required: true,
                    },
                    article_id: {
                         required: true
                    },
                    approved: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        $(".select2").select2()
    </script>
@endsection