@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{!! trans('admin_comment.show') !!}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_comment.fields.commentator_name') !!}</th>
                                <td>{{ $comment->commentator_name }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_comment.fields.commentator_email') !!}</th>
                                <td>{{ $comment->commentator_email }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_comment.fields.comment') !!}</th>
                                <td>{{ $comment->comment }}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_comment.fields.article') !!}</th>
                                <td>
                                    @if($comment->article)
                                        <a href="{{ route('admin.article.show',
                                        ['article' => $comment->article->id]) }}">
                                            {{ $comment->article->title }}
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_comment.fields.approved') !!}</th>
                                <td>
                                    @if($comment->approved)
                                        <a class="btn btn-xs btn-success"
                                           href="#">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-xs btn-danger"
                                           href="#">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.comment.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.comment.edit', ['comment' => $comment->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_comment.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
