@extends('layouts.admin')

@section('content')
    @include('admin.pages.index.week_history')
    @include('admin.pages.index.product_info')
    @include('admin.pages.index.shop_info')
    @include('admin.pages.index.blog_info')
@endsection