@extends('layouts.admin')

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">{{ trans('admin_attribute_set.all') }}</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
              </div>
                  <a class="pull-right btn btn-xs btn-success"
                   href="{{ route('admin.attribute_set.create') }}">
                    {{ trans('admin_attribute_set.create') }}
                </a>
            <div class="clearfix"></div>
           </div>

            <div class="widget-content">
                <div class="padd">
                    <div class="page-tables">
                        <table id="attribute_sets" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                                <th>{{ trans('admin_attribute_set.columns.id') }}</th>
                                <th>{{ trans('admin_attribute_set.columns.name') }}</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($attribute_sets as $attribute_set)
                            <tr>
                                <td>{{ $attribute_set->id }}</td>
                                <td>{{ $attribute_set->name }}</td>
                                <td>
                                    <a class="btn btn-xs btn-success"
                                        href="{{ route('admin.attribute_set.show',
                                        ['attribute_set' => $attribute_set->id]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-warning"
                                        href="{{ route('admin.attribute_set.edit',
                                        ['attribute_set' => $attribute_set->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a class="btn btn-xs btn-danger"
                                        href="#destroyModal" data-toggle="modal"
                                        data-url="{{ route('admin.attribute_set.destroy',
                                        ['attribute_set' => $attribute_set->id]) }}"
                                        data-text="{{ trans('common.destroy_attribute_set',
                                        ['id' => $attribute_set->id, 'email' => $attribute_set->email]) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                    <div class="widget-foot">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    @include('admin.partials.modals.destroy')
@endsection

@section('footer_script')
    <script type="text/javascript">
        $(function () {
            $('#attribute_sets').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: -1
                }]
            });
        });
    </script>
@endsection
