<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', trans('admin_attribute_set.fields.name')) !!}
        {!! Form::text('name', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_attribute_set.fields.name')]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('xml_product_tag', trans('admin_attribute_set.fields.xml_product_tag')) !!}
        {!! Form::text('xml_product_tag', null,
        ['class' => 'form-control', 'required' => 'required',
        'placeholder' => trans('admin_attribute_set.fields.xml_product_tag')]) !!}
    </div>
    <div class="widget">
        <div class="widget-head">
          <div class="pull-left">{{ trans('admin_attribute_set.attributes') }}</div>
          <div class="widget-icons pull-right">
            <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="widget-content">
            <div class="padd">
                <div class="form">
                    @if(isset($attribute_set))
                        @foreach($attribute_set->attributes as $attribute)
                            <div class="form-group attributes row">
                                <div class="col-sm-5">
                                    {!! Form::select("attributes[id][]", $attributes, $attribute->id,
                                    ['class' => 'form-control', 'placeholder' => 'Please select ...']) !!}
                                </div>
                                <div class="col-sm-5">
                                    {!! Form::text("attributes[xml_tag][]", $attribute->pivot->xml_tag,
                                    ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-danger attributes-remove"
                                        href="javascript:void(0);">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endif
                    {{-- @else --}}
                        <div class="form-group attributes row">
                            <div class="col-sm-5">
                                {!! Form::select("attributes[id][]", $attributes, '',
                                ['class' => 'form-control select2', 'placeholder' => 'Please select ...']) !!}
                            </div>
                            <div class="col-sm-5">
                                {!! Form::text("attributes[xml_tag][]", null,
                                ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-danger attributes-remove"
                                    href="javascript:void(0);">
                                    <i class="fa fa-minus"></i>
                                </a>
                                <a class="btn btn-success attributes-add"
                                    href="javascript:void(0);">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    {{-- @endif --}}
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <div class="box-footer">
        <a class="btn btn-default"
           href="{{ route('admin.attribute_set.index') }}">
            {{ trans('common.button.back') }}
        </a>
        {!! Form::submit(trans("common.button.submit"),
            ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

@section('head_styles')
    {!! Html::style('css/admin/select2/select2.min.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/admin/select2/select2.js') !!}
    <script>
        jQuery(function($) {
            $('.attribute-set-form').validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 255
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $('body').on('click', '.attributes-remove', function() {
                var $this = $(this);
                if ($('.attributes').size() > 1) {
                    $this.parents('.attributes').remove();
                }
            }).on('click', '.attributes-add', function() {
                var $this = $(this);
                $this.parents('.attributes').after(
                    $this.parents('.attributes').get(0).outerHTML
                );
            });
        });

//        $('select').select2();
    </script>
@endsection
