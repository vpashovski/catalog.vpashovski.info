@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                    <div class="pull-left">{{ $attribute_set->name }}</div>
                    <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <!-- Widget content -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>{!! trans('admin_attribute_set.fields.xml_product_tag') !!}</th>
                                <td>{{ $attribute_set->xml_product_tag }}</td>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-center">{!! trans('admin_attribute_set.attributes') !!}</th>
                            </tr>
                            <tr>
                                <th>{!! trans('admin_attribute_set.attributes') !!}</th>
                                <th>{!! trans('admin_attribute_set.xml_tags') !!}</th>
                            </tr>
                            @foreach($attribute_set->attributes as $attribute)
                                <tr>
                                    <td>{{ $attribute->name }}</td>
                                    <td>{{ $attribute->pivot->xml_tag }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="widget-foot">
                        <a class="btn  btn-sm btn-default"
                           href="{{ route('admin.attribute_set.index') }}">
                            {{ trans('common.button.back') }}
                        </a>
                        <a href="{{ route('admin.attribute_set.edit', ['attribute_set' => $attribute_set->id]) }}" class="btn  btn-sm btn-primary pull-right"><b>{!! trans('admin_attribute_set.edit') !!}</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection