@extends('layouts.2columns_right', ['title' => $article->title])

@section('content')
    <div class="posts">
        <!-- Each posts should be enclosed inside "entry" class" -->
        <!-- Post one -->
        <div class="entry">
            <!-- Meta details -->
            <div class="meta clearfix">
                <i class="fa fa-calendar"></i> {{ $article->updated_at->format('Y-m-d') }}
                <i class="fa fa-user"></i> {{ $article->user->name }}
                <i class="fa fa-folder-open"></i> <a href="{{ route('blog.section', ['section_slug' => $article->section->slug]) }}">{{ $article->section->title }}</a>
                <span class="pull-right"><i class="fa fa-comment"></i> {{ $article->approved_comments->count() }} {{ trans_choice('frontend_blog.comments', $article->approved_comments->count()) }}</span>
            </div>

            <!-- Thumbnail -->
            <div class="bthumb">
                <a href="{{ route('blog.article',
                        [
                            'section_slug' => $article->section->slug,
                            'article_slug' => $article->slug,
                        ]) }}">
                    <img src="{{ $article->image->url }}" alt="" class="img-responsive" />
                </a>
            </div>

            {!! $article->content !!}
        </div>

        <div class="well">
            <div class="article-rate-result"></div>
            <div class="pull-left">
                <h5>{{ trans('frontend_blog.rate') }}</h5>
                <div id="article_rateit" class="rateit" data-article_id="{{ $article->id }}" data-rateit-value="{{ $article->rating }}" data-rateit-resetable="false"></div>
            </div>
            <!-- Tags -->
            <div class="tags pull-right">
                <h5>{{ trans('frontend_blog.tags') }}</h5>
                @foreach($article->tags as $tag)
                    <a href="{{ route('blog.tag', ['tag_slug' => $tag->slug]) }}">{{ $tag->title }}</a>
                @endforeach
            </div>
            <div class="clearfix"></div>
        </div>

        <hr />

        <!-- Comment section -->

        <div class="comments well">
            <!-- Comment title -->
            <div class="title"><h4>{{ $article->approved_comments->count() }} {{ trans_choice('frontend_blog.comments', $article->approved_comments->count()) }}</h4></div>

            <ul class="comment-list">
                @foreach($article->approved_comments as $comment)
                    <li class="comment">
                        <!-- Author -->
                        <div class="comment-author">
                            <i class="fa fa-user"></i> {{ $comment->commentator_name }} @if($comment->commentator_email) ({{ $comment->commentator_email }}) @endif
                        </div>
                        <!-- Comment date -->
                        <div class="cmeta"><i class="fa fa-calendar"></i> {{ $comment->created_at->format('Y-m-d') }}</div>
                        <!-- Para -->
                        <i class="fa fa-comment"></i> {{ $comment->comment }}
                        <div class="clearfix"></div>
                    </li>
                @endforeach
            </ul>
        </div>

        <!-- Comment posting -->

        <div class="respond well">
            <div class="article-comment-result"></div>
            <!-- Form title -->
            <div class="title"><h4>{{ trans('frontend_blog.write_comment') }}</h4></div>
            <!-- Comment form -->
            {!! Form::open(['url' => route('blog.comment'), 'method' => 'POST', 'class' => 'form-horizontal comment-form']) !!}
            {!! Form::hidden('article_id', $article->id) !!}
            <div class="form-group">
                <label class="control-label col-md-3" for="name">{{ trans('frontend_blog.comment_form.name') }}</label>
                <div class="col-md-6">
                    {!! Form::text('commentator_name', null,
                    ['class' => 'form-control', 'required' => 'required',
                    'placeholder' => trans('frontend_blog.comment_form.name')]) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="email">{{ trans('frontend_blog.comment_form.email') }}</label>
                <div class="col-md-6">
                    {!! Form::text('commentator_email', null,
                    ['class' => 'form-control',
                    'placeholder' => trans('frontend_blog.comment_form.email')]) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="comment">{{ trans('frontend_blog.comment_form.comment') }}</label>
                <div class="col-md-6">
                    {!! Form::textarea('comment', null,
                         ['class' => 'form-control', 'rows' => '5', 'required' => 'required',
                         'placeholder' => trans('frontend_blog.comment_form.comment')]) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::submit(trans('frontend_blog.comment_button'),
                ['class' => 'btn btn-default col-sm-4 col-sm-offset-4']) !!}
            </div>
            {!! Form::close() !!}
        </div>

        <div class="clearfix"></div>
    </div>
@endsection

@section('head_styles')
    {!! Html::style('css/rateit.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.rateit.min.js') !!} <!-- RateIt - Star rating -->
    {!! Html::script('js/jquery.validate.min.js') !!}
    <script>
        jQuery(function($) {
            $('.comment-form').validate({
                rules: {
                    name: {
                        required: true
                    },
                    comment: {
                        required: true
                    }
                }
            });

            $('.comment-form').on('submit', function(e) {
                e.preventDefault();

                $('.article-comment-result').removeClass('alert').removeClass('alert-error').removeClass('alert-success');
                var $this = $(this);
                $.ajax({
                    method: $this.attr('method'),
                    url: $this.attr('action'),
                    data: $this.serialize(),
                    success: function(response) {
                        if (response.success) {
                            var div_class = 'alert alert-success';
                        } else {
                            var div_class = 'alert alert-danger';
                        }

                        $(':input[name="commentator_name"]').val('');
                        $(':input[name="commentator_email"]').val('');
                        $(':input[name="comment"]').val('');
                        $('.article-comment-result').addClass(div_class).text(response.message);
                    },
                    error: function(error) {
                        if (error.hasOwnProperty('responseJSON') &&
                            error.responseJSON.hasOwnProperty('message')) {
                            $('.article-comment-result').addClass('alert alert-danger').text(error.responseJSON.message);
                        }
                    },
                    complete: function() {
                        $(".article-comment-result").fadeTo(2000, 500).slideUp(500, function(){
                            $(this).slideUp(500);
                        });
                    }
                });
            });

            $('#article_rateit').bind('rated reset', function (e) {
                var ri = $(this);
                $('.article-rate-result').removeClass('alert').removeClass('alert-error').removeClass('alert-success');
                var value = ri.rateit('value');
                var article_id = ri.data('article_id');
                //maybe we want to disable voting?
                ri.rateit('readonly', true);

                $.ajax({
                    url:  '{{ route('blog.rate') }}',
                    data: { article_id: article_id, value: value, _token: '{{ csrf_token() }}' },
                    type: 'POST',
                    success: function (response) {
                        if (response.success) {
                            var div_class = 'alert alert-success';
                            ri.rateit('value', response.rating);
                        } else {
                            var div_class = 'alert alert-danger';
                            ri.rateit('value', '{{ $article->rating }}');
                        }

                        $('.article-rate-result').addClass(div_class).text(response.message);
                    },
                    error: function(error) {
                        if (error.hasOwnProperty('responseJSON') &&
                            error.responseJSON.hasOwnProperty('message')) {
                            ri.rateit('value', '{{ $article->rating }}');
                            $('.article-rate-result').addClass('alert alert-danger').text(error.responseJSON.message);
                        }
                    },
                    complete: function() {
                        $(".article-rate-result").fadeTo(2000, 500).slideUp(500, function(){
                            $(this).slideUp(500);
                        });
                    }
                });
            });
        });
    </script>
@endsection