@extends('layouts.2columns_right', ['title' => $title])

@section('content')
    <div class="posts">
        @forelse($articles as $article)
            @include('frontend.partials.article', ['article' => $article])
        @empty
            <h4 class="text-center">{{ trans('common.no_results') }}</h4>
        @endforelse

        {!! $articles->links() !!}
        <div class="clearfix"></div>
    </div>
@endsection
