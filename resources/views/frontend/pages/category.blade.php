@extends('layouts.2columns_left', ['title' => trans('common.index') . ' | ' . $category->name])

@section('content')
    <!-- Breadcrumb -->
    <ul class="breadcrumb">
        <li><a href="{{ route('index') }}">{{ trans('common.index') }}</a></li>
        @if ($category->parent)
            <li><a href="{{ route('category', ['slug' => $category->parent->slug]) }}">{{ $category->parent->name }}</a></li>
        @endif
        <li class="active">{{ $category->name }}</li>
    </ul>

    <!-- Title -->
    <h4 class="pull-left">{{ $category->name }}</h4>
    <div class="clearfix"></div>
    <div class="row">
        <!-- Item #1 -->
        @forelse($products as $product)
            <div class="col-md-4 col-sm-6">
                @include('frontend.partials.item', ['product' => $product])
            </div>
        @empty
            <h4 class="text-center">{{ trans('common.no_results') }}</h4>
        @endforelse
    </div>

    <div class="col-md-9 col-sm-9">
        <!-- Pagination -->
        {!! $products->links() !!}
    </div>
@endsection

@section('carousel_slider')
    <div class="container">
        <div class="rp">
            <!-- Recent News Starts -->
            <h4 class="title">{{ trans('common.choosen_products') }}</h4>
            <div class="recent-news block">
                @include('frontend.partials.carousel_slider', ['products' => $carousel_products])
            </div>
            <!-- Recent News Ends -->
        </div>
    </div>
@endsection