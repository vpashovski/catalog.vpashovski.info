@extends('layouts.1column_slider', ['title' => trans('frontend_index.index')])

@section('content')
    <div class="col-md-12">
        <h3 class="title">{{ trans('frontend_index.new_porducts') }}</h3>
    </div>
    @foreach($products as $product)
        <div class="col-md-3 col-sm-4">
            @include('frontend.partials.item', ['product' => $product])
        </div>
    @endforeach
@endsection

@section('carousel_slider')
    <div class="container">
        <div class="rp">
            <!-- Recent News Starts -->
            <h4 class="title">{{ trans('common.choosen_products') }}</h4>
            <div class="recent-news block">
                @include('frontend.partials.carousel_slider', ['products' => $carousel_products])
            </div>
            <!-- Recent News Ends -->
        </div>

    </div>
@endsection