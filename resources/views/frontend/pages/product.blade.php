@extends('layouts.2columns_left', ['title' => trans('common.index') . ' | ' . $product->getData(\App\Product::NAME)])

@section('content')
    <!-- Breadcrumbs -->
    <ul class="breadcrumb">
        <li><a href="{{ route('index') }}">{{ trans('common.index') }}</a></li>
        @if ($product->categories->first()->parent)
            <li>
                <a href="{{ route('category', ['slug' => $product->categories->first()->parent->slug]) }}">
                    {{ $product->categories->first()->parent->name }}
                </a>
            </li>
            <li>
                <a href="{{ route('category', ['slug' => $product->categories->first()->parent->slug, 'sub_slug' => $product->categories->first()->slug]) }}">
                    {{ $product->categories->first()->name }}
                </a>
            </li>
        @elseif ($product->categories->first())
            <li>
                <a href="{{ route('category', ['slug' => $product->categories->first()->slug]) }}">
                    {{ $product->categories->first()->name }}
                </a>
            </li>
        @endif
    </ul>


    <!-- Product details -->
    <div class="product-main">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Image. Flex slider -->
                <div class="product-slider">
                    <div class="product-image-slider flexslider">
                        <ul class="slides">
                            @if($product->image)
                                <li>
                                    <img src="{{ $product->image->url }}" alt=""/>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <!-- Title -->
                <h4 class="title">{{ $product->getData(\App\Product::NAME) }}</h4>
                <h5>{{ trans('frontend_product.price') }} : {{ number_format($product->getData(\App\Product::PRICE), 2) }} {{ trans('common.currency_symbol') }}</h5>
                <p>{{ trans('frontend_product.shop') }} :
                    <a href="{{ route('shop.view', ['shop_slug' => $product->shop->slug]) }}">{{ $product->shop->name }}</a>
                </p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a class="btn btn-default btn-sm" href="{{ $product->getData(\App\Product::AFF_LINK) }}">
                                    {{ trans('common.to_site') }}
                                </a>
                              </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br />

    <!-- Description, specs and review -->

    <ul class="nav nav-tabs">
        @if ($product->getData('description'))
            <li class="active"><a href="#description" data-toggle="tab">{{ trans('frontend_product.description') }}</a></li>
        @endif
        <li class="@if (!$product->getData('description')) active @endif"><a href="#attributes" data-toggle="tab">{{ trans('frontend_product.attributes') }}</a></li>
    </ul>

    <!-- Tab Content -->
    <div class="tab-content">
        <!-- Description -->
        @if ($product->getData('description'))
            <div class="tab-pane active" id="description">
                <h5>{{ $product->getData(\App\Product::NAME) }}</h5>
                {{ $product->getData('description') }}
            </div>
        @endif


        <!-- Sepcs -->
        <div class="tab-pane @if (!$product->getData('description')) active @endif" id="attributes">
            <table class="table table-striped tcart">
                <tbody>
                    @foreach($product->getData('*', true) as $key => $value)
                        <tr>
                            <td><strong>{{ $key }}</strong></td>
                            <td>{{ $value }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('carousel_slider')
    <div class="container">
        <div class="rp">
            <!-- Recent News Starts -->
            <h4 class="title">{{ trans('frontend_product.similar_products') }}</h4>
            <div class="recent-news block">
                @include('frontend.partials.carousel_slider', ['products' => $carousel_products])
            </div>
            <!-- Recent News Ends -->
        </div>
    </div>
@endsection