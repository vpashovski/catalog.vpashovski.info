@extends('layouts.1column', ['title' => trans('frontend_contact.index')])

@section('content')
    <div class="col-md-6">
        <!-- Contact form -->
        <h4 class="title">{{ trans('frontend_contact.contact_form') }}</h4>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @foreach ($errors->all() as $error)
                    <div><i class="icon fa fa-ban"></i> {{ $error }}</div>
                @endforeach
            </div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <div><i class="icon fa fa-check"></i> {{ session('success') }}</div>
            </div>
        @endif
        <div class="form">
            {!! Form::open(['method' => 'POST', 'url' => route('contact.send'), 'class' => 'form-horizontal contact-form']) !!}
            <!-- Name -->
                <div class="form-group">
                    <label class="control-label col-md-2" for="name">{{ trans('frontend_contact.contact_form_fields.name') }}</label>
                    <div class="col-md-9">
                        {!! Form::text('name', null,
                        ['class' => 'form-control', 'required' => 'required',
                        'placeholder' => trans('frontend_contact.contact_form_fields.name')]) !!}
                    </div>
                </div>
                <!-- Email -->
                <div class="form-group">
                    <label class="control-label col-md-2" for="email">{{ trans('frontend_contact.contact_form_fields.email') }}</label>
                    <div class="col-md-9">
                        {!! Form::email('email', null,
                        ['class' => 'form-control', 'required' => 'required',
                        'placeholder' => trans('frontend_contact.contact_form_fields.email')]) !!}
                    </div>
                </div>

                <!-- Comment -->
                <div class="form-group">
                    <label class="control-label col-md-2" for="comment">{{ trans('frontend_contact.contact_form_fields.comment') }}</label>
                    <div class="col-md-9">
                        {!! Form::textarea('comment', null,
                        ['class' => 'form-control', 'rows' => '3',
                        'placeholder' => trans('frontend_contact.contact_form_fields.comment')]) !!}
                    </div>
                </div>
                <!-- Buttons -->
                <div class="form-group">
                    <!-- Buttons -->
                    <div class="col-md-9 col-md-offset-2">
                        <button type="submit" class="btn btn-default">{{ trans('frontend_contact.contact_button') }}</button>
                        <button type="reset" class="btn btn-default">{{ trans('frontend_contact.reset_button') }}</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <hr />
        <div class="center">
            <!-- Social media icons -->
            <strong>{{ trans('frontend_contact.get_in_touch') }}</strong>
            <div class="social">
                @if (Setting::get('facebook'))
                    <a href="{{ Setting::get('facebook') }}"><i class="fa fa-facebook facebook"></i></a>
                @endif
                @if (Setting::get('twitter'))
                    <a href="{{ Setting::get('twitter') }}"><i class="fa fa-twitter twitter"></i></a>
                @endif
                @if (Setting::get('linkedin'))
                    <a href="{{ Setting::get('linkedin') }}"><i class="fa fa-linkedin linkedin"></i></a>
                @endif
                @if (Setting::get('google_plus'))
                    <a href="{{ Setting::get('google_plus') }}"><i class="fa fa-google-plus google-plus"></i></a>
                @endif
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <h4 class="title">{{ trans('frontend_contact.google_map') }}</h4>
        <!-- Google maps -->
        @if (Setting::get('google_map'))
            <div class="gmap">
                <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                {!! Setting::get('google_map') !!}
            </div>
        @endif
        <hr />
        <!-- Address section -->
        <h4 class="title">{{ trans('frontend_contact.address') }}</h4>
        <div class="address">
            <address>
                {!! nl2br(Setting::get('full_contact_address')) !!}
            </address>

            <address>
                <!-- Name -->
                <strong>{{ Setting::get('contact_name') }}</strong><br>
                <!-- Email -->
                <i class="fa fa-envelope-o"></i> &nbsp; <a href="mailto:{{ Setting::get('contact_email') }}">{{ Setting::get('contact_email') }}</a>
            </address>
        </div>
    </div>
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.validate.min.js') !!}
    {!! Html::script('js/jquery.validate.messages_bg.min.js') !!}
    <script>
        jQuery(function($) {
            $('.contact-form').validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 255
                    },
                    email: {
                        required: true,
                        email: true,
                        maxlength: 255
                    },
                    comment: {
                        required: true,
                        maxlength: 255
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
@endsection