@extends('layouts.1column', ['title' => $page->title])

@section('content')
    <div class="col-md-12">
        <div class="well">
            {!! $page->content !!}
        </div>
    </div>
@endsection
