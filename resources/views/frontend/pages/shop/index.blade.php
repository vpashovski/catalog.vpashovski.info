@extends('layouts.2columns_left', ['title' => trans('frontend_shop.index') . ' | ' . trans('frontend_shop.all_shops')])

@section('content')
    <!-- Breadcrumb -->
    <ul class="breadcrumb">
        <li><a href="{{ route('index') }}">{{ trans('common.index') }}</a></li>
        <li class="active">{{ trans('frontend_shop.all_shops') }}</li>
    </ul>

    <!-- Title -->
    <h4 class="pull-left">{{ trans('frontend_shop.all_shops') }}</h4>
    <div class="clearfix"></div>
    <div class="row">
        @forelse($shops as $shop)
            <div class="col-md-4 col-sm-6">
                <div class="item" style="">
                    <!-- Item image -->
                    <div class="item-image">
                        <a href="{{ route('shop.view', ['shop_slug' => $shop->slug]) }}"><img src="{{ $shop->image->url }}" alt="" /></a>
                    </div>
                    <!-- Item details -->
                    <div class="item-details">
                        <h5>
                            <a href="{{ route('shop.view', ['shop_slug' => $shop->slug]) }}">{{ $shop->name }}</a>
                        </h5>
                        <hr />
                        <!-- Price -->
                        {{--<div class="item-price pull-left"></div>--}}
                        <!-- Add to cart -->
                        <div class="button pull-right">
                            <a href="{{ route('shop.view', ['shop_slug' => $shop->slug]) }}">{{ trans('frontend_shop.view') }}</a></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        @empty
            <h4 class="text-center">{{ trans('common.no_results') }}</h4>
        @endforelse

        <div class="col-md-9 col-sm-9">
            <!-- Pagination -->
            {!! $shops->links() !!}
        </div>
    </div>
@endsection
