@extends('layouts.2columns_left', ['title' => trans('common.index') . ' | ' . $shop->name])

@section('content')
    <!-- Breadcrumb -->
    <ul class="breadcrumb">
        <li><a href="{{ route('index') }}">{{ trans('common.index') }}</a></li>
        <li class="active"><a href="{{ route('shop.view', ['shop_slug' => $shop->slug]) }}">{{ $shop->name }}</a></li>
        <li class="active">{{ trans('frontend_shop.products') }}</li>
    </ul>

    <!-- Title -->
    <h4 class="pull-left">{{ $shop->name }}</h4>
    <div class="clearfix"></div>
    <div class="row">
        <!-- Item #1 -->
        @forelse($products as $product)
            <div class="col-md-4 col-sm-6">
                @include('frontend.partials.item', ['product' => $product])
            </div>
        @empty
            <h4 class="text-center">{{ trans('common.no_results') }}</h4>
        @endforelse

        <div class="col-md-9 col-sm-9">
            <!-- Pagination -->
            {!! $products->links() !!}
        </div>
    </div>
@endsection