@extends('layouts.2columns_left', ['title' => trans('frontend_shop.index') . ' | ' . $shop->name])

@section('content')
    <!-- Breadcrumbs -->
    <ul class="breadcrumb">
        <li><a href="{{ route('index') }}">{{ trans('common.index') }}</a></li>
        <li class="active">{{ $shop->name }}</li>
    </ul>


    <!-- Product details -->
    <div class="product-main">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Image. Flex slider -->
                <div class="product-slider">
                    <div class="product-image-slider flexslider">
                        <ul class="slides">
                            @foreach($shop->images as $image)
                                <li>
                                    <img src="{{ $image->url }}" alt=""/>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <!-- Title -->
                <h4 class="title">
                    @if($shop->shop_aff_url)
                        <a href="{{ $shop->shop_aff_url }}">{{ $shop->name }}</a>
                    @else
                        <a href="{{ $shop->shop_url }}">{{ $shop->name }}</a>
                    @endif
                </h4>

                <p>{{ trans('frontend_shop.main_category') }} : {{ $shop->category->name }}</p>
                <p>{{ trans('frontend_shop.products_count') }} : {{ $shop->products->count() }}</p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-btn">
                                @if($shop->shop_aff_url)
                                    <a class="btn btn-default btn-sm" href="{{ $shop->shop_aff_url }}">
                                        {{ trans('common.to_site') }}
                                   </a>
                                @else
                                    <a class="btn btn-default btn-sm" href="{{ $shop->shop_url }}">
                                       {{ trans('common.to_site') }}
                                    </a>
                                @endif
                              </span>
                        </div>
                    </div>
                </div>
                <a href="{{ route('shop.products', ['shop_slug' => $shop->slug]) }}">{{ trans('frontend_shop.see_all_products') }}</a>

                <div class="well">
                    <div class="shop-rate-result"></div>
                    <div class="pull-left">
                        <h5>{{ trans('frontend_shop.rate') }}</h5>
                        <div id="shop_rateit" class="rateit" data-shop_id="{{ $shop->id }}" data-rateit-value="{{ $shop->rating }}" data-rateit-resetable="false"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('carousel_slider')
    <div class="container">
        <div class="rp">
            <h4 class="title">{{ trans('frontend_shop.products') }}</h4>
            <div class="recent-news block">
                @include('frontend.partials.carousel_slider', ['products' => $carousel_products])
            </div>
        </div>
    </div>
@endsection

@section('head_styles')
    {!! Html::style('css/rateit.css') !!}
@endsection

@section('footer_script')
    {!! Html::script('js/jquery.rateit.min.js') !!}
    <script>
        jQuery(function($) {
            $('#shop_rateit').bind('rated reset', function (e) {
                var ri = $(this);
                $('.shop-rate-result').removeClass('alert').removeClass('alert-error').removeClass('alert-success');
                var value = ri.rateit('value');
                var shop_id = ri.data('shop_id');
                //maybe we want to disable voting?
                ri.rateit('readonly', true);

                $.ajax({
                    url:  '{{ route('shop.rate') }}',
                    data: { shop_id: shop_id, value: value, _token: '{{ csrf_token() }}' },
                    type: 'POST',
                    success: function (response) {
                        if (response.success) {
                            var div_class = 'alert alert-success';
                            ri.rateit('value', response.rating);
                        } else {
                            var div_class = 'alert alert-danger';
                            ri.rateit('value', '{{ $shop->rating }}');
                        }

                        $('.shop-rate-result').addClass(div_class).text(response.message);
                    },
                    error: function(error) {
                        if (error.hasOwnProperty('responseJSON') &&
                            error.responseJSON.hasOwnProperty('message')) {
                            ri.rateit('value', '{{ $shop->rating }}');
                            $('.shop-rate-result').addClass('alert alert-danger').text(error.responseJSON.message);
                        }
                    },
                    complete: function() {
                        $(".shop-rate-result").fadeTo(2000, 500).slideUp(500, function(){
                            $(this).slideUp(500);
                        });
                    }
                });
            });
        });
    </script>
@endsection