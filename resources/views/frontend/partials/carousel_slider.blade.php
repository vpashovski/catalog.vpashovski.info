<!-- Recent Item -->
<div class="recent-item">
    <div class="custom-nav">
        <a class="prev"><i class="fa fa-chevron-left br-lblue"></i></a>
        <a class="next"><i class="fa fa-chevron-right br-lblue"></i></a>
    </div>
    <div id="owl-recent" class="owl-carousel">
        <!-- Item -->
        @foreach($products as $product)
            <div class="item">
                <a href="{{ route('product', ['id' => $product->id]) }}">
                    @if($product->image)
                        <img src="{{ $product->image->url }}" alt="" class="img-responsive" />
                    @else
                        {!! Html::image('uploads/images/no-image.png',
                         'Product Image', ['width' => 140, 'height' => 140, 'class' => 'img-responsive']) !!}
                    @endif
                </a>
                <!-- Heading -->
                <h4>
                    <a href="{{ route('product', ['id' => $product->id]) }}">
                        {{ str_limit($product->getData(\App\Product::NAME), 15) }}
                        <span class="pull-right">
                            {{ number_format($product->getData(\App\Product::PRICE), 2) }} {{ trans('common.currency_symbol') }}
                        </span>
                    </a>
                </h4>
                <div class="clearfix"></div>
                <!-- Paragraph -->
                <p class="item-name">{{ str_limit($product->getData('description'), 25) }}</p>
            </div>
        @endforeach
    </div>
</div>