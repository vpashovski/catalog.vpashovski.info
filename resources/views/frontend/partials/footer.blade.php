<!-- Footer starts -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="widget">
                            <h5>{{ trans('common.contact') }}</h5>
                            <hr />
                            <div class="social">
                                @if (Setting::get('facebook'))
                                    <a href="{{ Setting::get('facebook') }}"><i class="fa fa-facebook facebook"></i></a>
                                @endif
                                @if (Setting::get('twitter'))
                                    <a href="{{ Setting::get('twitter') }}"><i class="fa fa-twitter twitter"></i></a>
                                @endif
                                @if (Setting::get('linkedin'))
                                    <a href="{{ Setting::get('linkedin') }}"><i class="fa fa-linkedin linkedin"></i></a>
                                @endif
                                @if (Setting::get('google_plus'))
                                    <a href="{{ Setting::get('google_plus') }}"><i class="fa fa-google-plus google-plus"></i></a>
                                @endif
                            </div>
                            <hr />
                            <i class="fa fa-home"></i> &nbsp; {!! Setting::get('contact_address') !!}
                            <hr />
                            <i class="fa fa-phone"></i> &nbsp;&nbsp; {!! Setting::get('contact_phone') !!}
                            <hr />
                            <i class="fa fa-envelope-o"></i> &nbsp; <a href="mailto:{{ Setting::get('contact_email') }}">{{ Setting::get('contact_email') }}</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="widget">
                            <h5>{{ trans('common.about_us') }}</h5>
                            <hr />
                            <p>{!! nl2br(Setting::get('about_us')) !!}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="widget">
                            <h5>{{ trans('common.pages') }}</h5>
                            <hr />
                            <div class="two-col">
                                @foreach($footer_pages as $footer_page)
                                <div class="@if($loop->index <= $loop->count / 2) col-left @else col-right @endif">
                                    <ul>
                                        <li>
                                            <a href="{{ route('page', ['url' => $footer_page->url]) }}">
                                                {{ $footer_page->title }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <!-- Copyright info -->
                <p class="copy">Copyright &copy; {{ \Carbon\Carbon::now()->format('Y') }} |
                    <a href="{{ route('index') }}">{{ trans('common.index') }}</a>
                    - <a href="{{ route('index') }}">{{ trans('common.home') }}</a>
                    | <a href="{{ route('contact') }}">{{ trans('common.contact_us') }}</a></p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</footer>
<!--/ Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span>

<!-- Javascript files -->
{!! Html::script('js/jquery.js') !!} <!-- jQuery -->
{!! Html::script('js/bootstrap.min.js') !!} <!-- Bootstrap -->
{!! Html::script('js/jquery-ui.min.js') !!} <!-- jQuery UI -->

{!! Html::script('js/frontend/owl.carousel.min.js') !!}
{!! Html::script('js/frontend/filter.js') !!}
{!! Html::script('js/frontend/jquery.flexslider-min.js') !!}
{!! Html::script('js/frontend/respond.min.js') !!}
{!! Html::script('js/frontend/html5shiv.js') !!}
{!! Html::script('js/frontend/custom.js') !!}

@yield('footer_script')