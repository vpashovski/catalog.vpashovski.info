<div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
        <ul class="nav navbar-nav">
            @foreach($menus as $menu)
                <li class="@if(request()->is($menu->url . '*')) active @endif"><a href="{{ url($menu->url) }}">{{ $menu->title }}</a></li>
            @endforeach
        </ul>
    </nav>
</div>