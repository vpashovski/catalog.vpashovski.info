@if (isset($parent_category))
    <h5 class="title">{{ trans('common.categories') }}</h5>
    <!-- Sidebar navigation -->
    <nav>
        <ul id="nav">
            @foreach($parent_category->childrens as $children)
                <li>
                    <a href="{{ route('category', ['slug' => $parent_category->slug, 'sub_slug' => $children->slug]) }}">
                        {{ $children->name }}
                    </a>
                </li>
            @endforeach

        </ul>
    </nav>
    <br />
    <!-- Sidebar items (featured items)-->
@endif

@if (isset($left_shops))
    <h5 class="title">{{ trans('common.shops') }}</h5>
    <!-- Sidebar navigation -->
    <nav>
        <ul id="nav">
            @foreach($left_shops as $left_shop)
                <li>
                    <a href="{{ route('shop.view', ['shop_slug' => $left_shop->slug]) }}">
                        {{ $left_shop->name }}
                    </a>
                </li>
            @endforeach

        </ul>
    </nav>
    <br />
    <!-- Sidebar items (featured items)-->
@endif

<div class="sidebar-items">
    <h5 class="title">Featured Items</h5>

    <!-- Item #1 -->
    <div class="sitem">
        <!-- Don't forget the class "onethree-left" and "onethree-right" -->
        <div class="onethree-left">
            <!-- Image -->
            <a href="single-item.html"><img src="{{ asset('img/photos/2.png') }}" alt="" class="img-responsive" /></a>
        </div>
        <div class="onethree-right">
            <!-- Title -->
            <a href="single-item.html">HTC One V</a>
            <!-- Para -->
            <p>Aenean ullamcorper justo tincidunt justo aliquet.</p>
            <!-- Price -->
            <p class="bold">$199</p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>