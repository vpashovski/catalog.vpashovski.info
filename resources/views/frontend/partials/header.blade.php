<div class="container">
    <div class="row">
        <div class="col-md-4">
            <!-- Logo. Use class "color" to add color to the text. -->
            <div class="logo">
                <h1>
                    <a href="{{ route('index') }}">
                        <span class="color bold">
                          @if(Setting::get('site_name'))
                                {{ Setting::get('site_name') }}
                            @else
                                {{ trans('common.index') }}
                            @endif
                        </span>
                    </a>
                </h1>
                <p class="meta">
                    @if(Setting::get('site_description'))
                        {{ Setting::get('site_description') }}
                    @else
                        {{ trans('common.desc') }}
                    @endif
                </p>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4">
        {!! Form::open(['method' => 'GET', 'url' => route('search')]) !!}
            <div class="input-group">
                <input type="text" name="q" class="form-control" value="{{ app('request')->input('q') }}" id="header_search" placeholder="{{ trans('common.search_products') }}">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">{{ trans('common.search') }}</button>
                </span>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>