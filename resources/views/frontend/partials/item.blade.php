<div class="item" style="">
    <!-- Item image -->
    <div class="item-image">
        <a href="{{ route('product', ['id' => $product->id]) }}">
            @if($product->image)
                <img src="{{ $product->image->url }}" alt="{{ str_limit($product->getData(\App\Product::NAME), 15) }}" /></a>
            @else
                {!! Html::image('uploads/images/no-image.png',
                 'Product Image', ['width' => 140, 'height' => 140]) !!}
            @endif

    </div>
    <!-- Item details -->
    <div class="item-details">
        <!-- Name -->
        <!-- Use the span tag with the class "ico" and icon link (hot, sale, deal, new) -->
        <h5><a href="{{ route('product', ['id' => $product->id]) }}">{{ str_limit($product->getData(\App\Product::NAME), 15) }}</a>
            @if ($product->created_at->gt(\Carbon\Carbon::now()->subDays(config('constant.days_older_than'))))
                <span class="ico"><img src="{{ asset('img/new.png') }}" alt="" /></span>
            @endif
        </h5>
        <div class="clearfix"></div>
        <!-- Para. Note more than 2 lines. -->
        <p class="item-name">{{ str_limit($product->getData('description'), 50) }}</p>
        <hr />
        <!-- Price -->
        <div class="item-price pull-left">{{ number_format($product->getData(\App\Product::PRICE), 2) }} {{ trans('common.currency_symbol') }}</div>
        <!-- Add to cart -->
        <div class="button pull-right"><a href="{{ $product->getData(\App\Product::AFF_LINK) }}">{{ trans('common.to_site') }}</a></div>
        <div class="clearfix"></div>
    </div>
</div>