<div class="sidebar">
    <div class="widget">
        <h4>{{ trans('common.searching') }}</h4>
        {!! Form::open(['url' => route('blog.search'), 'method' => 'GET', 'class' => 'form-inline search-form']) !!}
        <div class="form-group">
            {!! Form::text('query', null,
            ['class' => 'form-control',
            'placeholder' => trans('common.searching')]) !!}
        </div>

        {!! Form::submit(trans('common.search'),
            ['class' => 'btn btn-default']) !!}
        {!! Form::close() !!}
    </div>

    @if (isset($right_sections))
        <!-- Widget -->
        <div class="widget">
            <h4>{{ trans('common.sections') }}</h4>
            <ul>
                @foreach($right_sections as $right_section)
                    <li>
                        <a href="{{ route('blog.section', ['section_slug' => $right_section->slug]) }}">
                            {{ $right_section->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    @if($right_recent_articles)
        <div class="widget">
            <h4>{{ trans('common.recent_articles') }}</h4>
            <ul>
                @foreach($right_recent_articles as $right_recent_article)
                    <li>
                        <a href="{{ route('blog.article',
                            [
                                'section_slug' => $right_recent_article->section->slug,
                                'article_slug' => $right_recent_article->slug,
                            ]) }}">
                            {{ $right_recent_article->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
</div>