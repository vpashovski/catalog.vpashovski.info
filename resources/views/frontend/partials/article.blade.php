<div class="entry">
    <h2>
        <a href="{{ route('blog.article',
                        [
                            'section_slug' => $article->section->slug,
                            'article_slug' => $article->slug,
                        ]) }}">
            {{ $article->title }}
        </a>
    </h2>
    <!-- Meta details. Calendar, Author, Category and comments-->
    <div class="meta clearfix">
        <i class="fa fa-calendar"></i> {{ $article->updated_at->format('Y-m-d') }}
        <i class="fa fa-user"></i> {{ $article->user->name }}
        <i class="fa fa-folder-open"></i> <a href="{{ route('blog.section', ['section_slug' => $article->section->slug]) }}">{{ $article->section->title }}</a>
        <span class="pull-right"><i class="fa fa-comment"></i> {{ $article->approved_comments->count() }} {{ trans_choice('frontend_blog.comments', $article->approved_comments->count()) }}</span>
    </div>

    <!-- Thumbnail -->
    <div class="bthumb">
        <a href="{{ route('blog.article',
                        [
                            'section_slug' => $article->section->slug,
                            'article_slug' => $article->slug,
                        ]) }}">
            <img src="{{ $article->image->url }}" alt="" class="img-responsive" />
        </a>
    </div>

    {!! $article->description !!}
    <div class="button">
        <a href="{{ route('blog.article',
                        [
                            'section_slug' => $article->section->slug,
                            'article_slug' => $article->slug,
                        ]) }}">
            {{ trans('common.read_more') }}
        </a>
    </div>
</div>