<div class="container flex-main">
    <div class="row">
        <div class="col-md-12">
            <div class="flex-image flexslider">
                <ul class="slides">
                    <!-- Each slide should be enclosed inside li tag. -->
                    <!-- Slide #1 -->
                    <li>
                        <!-- Image -->
                        <img src="img/photos/slider1.jpg" alt=""/>
                        <!-- Caption -->
                        <div class="flex-caption">
                            <!-- Title -->
                            <h3>Levi's T-Shirt - <span class="color">Just $49</span></h3>
                            <!-- Para -->
                            <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                            <div class="button">
                                <a href="single-item.html">Buy Now</a>
                            </div>
                        </div>
                    </li>

                    <!-- Slide #2 -->
                    <li>
                        <img src="img/photos/slider2.jpg" alt=""/>
                        <div class="flex-caption">
                            <!-- Title -->
                            <h3>Denim Jeans - <span class="color">Just $149</span></h3>
                            <!-- Para -->
                            <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                            <div class="button">
                                <a href="single-item.html">Buy Now</a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <img src="img/photos/slider3.jpg" alt=""/>
                        <div class="flex-caption">
                            <!-- Title -->
                            <h3>Polo Shirts - <span class="color">Just $79</span></h3>
                            <!-- Para -->
                            <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                            <div class="button">
                                <a href="single-item.html">Buy Now</a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <img src="img/photos/slider4.jpg" alt=""/>
                        <div class="flex-caption">
                            <!-- Title -->
                            <h3>Raymonds Suitings - <span class="color">Just $449</span></h3>
                            <!-- Para -->
                            <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                            <div class="button">
                                <a href="single-item.html">Buy Now</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>