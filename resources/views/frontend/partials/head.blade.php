<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- Title and other stuffs -->
<title>{{ isset($title) ? $title : config('app.name', 'Online Catalog') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon/favicon.png">

<!-- Stylesheets -->
{!! Html::style('css/bootstrap.min.css') !!}
<!-- Font awesome icon -->
{!! Html::style('css/font-awesome.min.css') !!}
{!! Html::style('css/font-awesome-animation.min.css') !!}

{!! Html::style('css/frontend/flexslider.css') !!}
{!! Html::style('css/frontend/owl.carousel.css') !!}
{!! Html::style('css/frontend/style.css') !!}
{!! Html::style('css/frontend/sidebar-nav.css') !!}

@php ($style = !is_null(Setting::get('frontend_color')) ? Setting::get('frontend_color') : 'blue')
{!! Html::style("css/frontend/$style.css") !!}

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon/favicon.png">

@yield('head_styles')