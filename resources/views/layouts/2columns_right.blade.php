<!DOCTYPE html>
<html lang="en">
    <head>
        @include('frontend.partials.head')
    </head>

    <body>
        <!-- Header starts -->
        <header>
            @include('frontend.partials.header')
        </header>
        <!--/ Header ends -->

        <!-- Navigation -->
        <div class="navbar bs-docs-nav" role="banner">
            @include('frontend.partials.navigation')
        </div>
        <!--/ Navigation End -->

        <!-- Page heading starts -->

        @if(isset($title))
            <div class="page-head">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>{{ $title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <!-- Page Heading ends -->

        <!-- Page content starts -->
        <div class="content blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        @yield('content')
                    </div>
                    <div class="col-md-4">
                        <!-- Sidebar -->
                        @include('frontend.partials.right_sidebar')
                    </div>
                </div>
            </div>
        </div>

        <!-- Page content ends -->

        <!-- Newsletter starts -->
        {{--@include('frontend.partials.newsletter')--}}
        <!--/ Newsletter ends -->

        @include('frontend.partials.footer')
    </body>
</html>