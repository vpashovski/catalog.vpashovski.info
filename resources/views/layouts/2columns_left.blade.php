<!DOCTYPE html>
<html lang="en">
    <head>
        @include('frontend.partials.head')
    </head>

    <body>
        <!-- Header starts -->
        <header>
            @include('frontend.partials.header')
        </header>
        <!--/ Header ends -->

        <!-- Navigation -->
        <div class="navbar bs-docs-nav" role="banner">
            @include('frontend.partials.navigation')
        </div>
        <!--/ Navigation End -->

        <!-- Items -->
        <div class="items">
            <div class="container">
                <div class="row">

                    <!-- Sidebar -->
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        @include('frontend.partials.left_sidebar')
                    </div>

                    <!-- Main content -->
                    <div class="col-md-9 col-sm-9">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <!-- Owl Carousel Starts -->
        @yield('carousel_slider')
        <!-- Owl Carousel Ends -->

        <!-- Newsletter starts -->
            {{--@include('frontend.partials.newsletter')--}}
        <!--/ Newsletter ends -->

        @include('frontend.partials.footer')
    </body>
</html>