<!DOCTYPE html>
<html lang="en">
    <head>
        @include('frontend.partials.head')
    </head>

    <body>
        <!-- Header starts -->
        <header>
            @include('frontend.partials.header')
        </header>
            <!--/ Header ends -->

        <!-- Navigation -->
        <div class="navbar bs-docs-nav" role="banner">
            @include('frontend.partials.navigation')
        </div>
        <!--/ Navigation End -->


        <!-- Flex Slider starts -->
            @include('frontend.partials.flex_slider')
        <!-- Flex slider ends -->

        <!-- Promo box starts -->
            @include('frontend.partials.promo')
        <!-- Promo box ends -->

        <!-- Items -->
        <div class="items">
            <div class="container">
                <div class="row">
                    @yield('content')
                </div>
            </div>
        </div>
        <!--/ Items end -->

        <!-- Owl Carousel Starts -->
        @yield('carousel_slider')
        <!-- Owl Carousel Ends -->

        <!-- Newsletter starts -->
{{--        @include('frontend.partials.newsletter')--}}
        <!--/ Newsletter ends -->

        @include('frontend.partials.footer')
    </body>
</html>