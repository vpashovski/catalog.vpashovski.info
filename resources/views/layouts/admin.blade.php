<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.partials.head')
    </head>

    <body>
        @include('admin.partials.fixed_navbar')
        @include('admin.partials.header')

        <!-- Main content starts -->

        <div class="content">
            @include('admin.partials.sidebar')

            <!-- Main bar -->
            <div class="mainbar">
                @include('admin.partials.breadcrumbs')

                <!-- Matter -->
                <div class="matter">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                @include('admin.partials.success')
                                @include('admin.partials.errors')
                            </div>
                        </div>
                        @yield('content')
                    </div>
                </div>
                <!-- Matter ends -->
            </div>
            <!-- Mainbar ends -->
            <div class="clearfix"></div>
        </div>
        <!-- Content ends -->

        @include('admin.partials.footer')
        @yield('footer_script')
    </body>
</html>
