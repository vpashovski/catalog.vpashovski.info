<!DOCTYPE html>
<html lang="en">
    <head>
        @include('frontend.partials.head')
    </head>

    <body>
        <!-- Header starts -->
        <header>
            @include('frontend.partials.header')
        </header>
        <!--/ Header ends -->

        <!-- Navigation -->
        <div class="navbar bs-docs-nav" role="banner">
            @include('frontend.partials.navigation')
        </div>
        <!--/ Navigation End -->

        @if(isset($title))
            <div class="page-head">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>{{ $title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <!-- Page Heading ends -->

        <!-- Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <!-- Newsletter starts -->
        {{--        @include('frontend.partials.newsletter')--}}
        <!--/ Newsletter ends -->

        @include('frontend.partials.footer')
    </body>
</html>